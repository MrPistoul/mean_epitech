var app = angular
    .module('GhostTrain', ['ngRoute', 'LocalStorageModule', 'ngFileUpload', 'hashtagify', 'ui.bootstrap', 'ui-notification', 'ngMaterial', 'ngAnimate', 'ngMdIcons','btford.socket-io'])
    .config(['$routeProvider', '$locationProvider', 'localStorageServiceProvider', 'NotificationProvider',
        function($routeProvider, $locationProvider, localStorageServiceProvider, NotificationProvider) {
            $routeProvider
                .when('/login', {
                    templateUrl: '../views/login.html',
                    controller: 'loginCtrl'
                })
                .when('/register', {
                    templateUrl: '../views/register.html',
                    controller: 'registerCtrl'
                })
                .when('/logout', {
                    templateUrl: '../views/login.html',
                    controller: 'logoutCtrl'
                })
                .when('/wall', {
                    templateUrl: '../views/wall.html',
                    controller: 'wallCtrl'
                })
                .when('/friends', {
                    templateUrl: '../views/friends.html',
                    controller: 'friendsCtrl'
                })
                .when('/passengers', {
                    templateUrl: '../views/passengers.html',
                    controller: 'passengersCtrl'
                })
                .when('/passenger', {
                    templateUrl: '../views/passenger.html'
                })
                .when('/messages', {
                    templateUrl: '../views/messages.html',
                    controller: 'messagesCtrl'
                })
                .when('/messages/archive', {
                    templateUrl: '../views/messages-archive.html',
                    controller: 'archivedMessagesCtrl'
                })
                .when('/messages/private', {
                    templateUrl: '../views/messages-private.html',
                    controller: 'privateMessageCtrl'
                })
                .when('/albums', {
                    templateUrl: '../views/albums.html',
                    controller: 'albumsCtrl'
                })
                .when('/album', {
                    templateUrl: '../views/album.html',
                    controller: 'albumCtrl'
                })
                .when('/profile', {
                    templateUrl: '../views/profile.html',
                    controller: 'profileCtrl'
                })
                .when('/password', {
                    templateUrl: '../views/password.html',
                    controller: 'profileCtrl'
                })
                .when('/admin/users', {
                    templateUrl: '../views/admin-users.html',
                    controller: 'adminUsersCtrl'
                })
                .when('/admin/posts', {
                    templateUrl: '../views/admin-posts.html',
                    controller: 'adminPostsCtrl'
                })
                .when('/chat', {
                    templateUrl: '../views/chat.html',
                    controller: 'ChatCtrl'
                })
                .otherwise({
                    redirectTo: '/login'
                });

            localStorageServiceProvider
                .setPrefix('GhostTrain')
                .setStorageType('sessionStorage')
                .setStorageCookie(30, '/')
                .setNotify(true, true);

            $locationProvider.html5Mode(true);

            NotificationProvider.setOptions({
                delay: 10000,
                startTop: 20,
                startRight: 10,
                verticalSpacing: 20,
                horizontalSpacing: 20,
                positionX: 'right',
                positionY: 'bottom'
            });
    }]);
