app.controller('loginCtrl', ['$rootScope', '$scope', '$http', 'localStorageService', '$location', 'Notification',
    function($rootScope, $scope, $http, localStorageService, $location, Notification) {
        var checkStorage = localStorageService.isSupported;
        var checkCookie = localStorageService.cookie.isSupported;
        var socket = io.connect();

        if (checkStorage) {
            if (localStorageService.get("user")) {
                $rootScope.user = localStorageService.get("user");
                $location.path('/wall');
            } else if (checkCookie && localStorageService.cookie.get("user")) {
                $rootScope.user = localStorageService.cookie.get("user");
                $location.path('/wall');
            }
        }

        $scope.login = function() {
            var data = { login: $scope.loginEmail, password: $scope.loginPassword };
            var remember = $scope.rememberMe;
            $http.post('/login', data)
                .success(function(data, status) {
                    $rootScope.user = data.user;
                    Notification({ message: data.message }, 'success' );
                    $scope.loginEmail = '';
                    $scope.loginPassword = '';
                    var redirect = function($location) {
                        $location.path('/wall');
                        var missive = { user: $rootScope.user };
                        socket.emit('login', missive);
                    };
                    var setUser = function($scope, checkStorage, localStorageService, $location, data, redirect) {
                        if(checkStorage) {
                            localStorageService.set("user", data.user);
                        }
                        if(remember && checkCookie) {
                            localStorageService.cookie.set("user", data.user);
                        }
                        redirect($location);
                    };
                    setUser($scope, checkStorage, localStorageService, $location, data, redirect);

                    // SOCKET LISTENING FOR NOTIFICATION
                    socket.on('notification', function(data) {
                        Notification({ title: data.title, message: data.message }, data.type );
                    });

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });

                    // GO BACK
                    $rootScope.goBack = function(){
                        history.back();
                    }
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                    $scope.loginPassword = '';
                });
        };

        $scope.goToRegister = function() {
            $location.path('/register');
        };
    }]);
