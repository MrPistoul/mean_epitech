app.controller('logoutCtrl', ['$rootScope', 'localStorageService', '$location', 'Notification', '$http',
    function($rootScope, localStorageService, $location, Notification, $http) {
        var checkStorage = localStorageService.isSupported;
        var checkCookie = localStorageService.cookie.isSupported;
        var data = { userId: $rootScope.user._id};

        var logout = function(localStorageService, $location) {
            $rootScope.wall = null;
            $rootScope.user = null;
            Notification({ message: "you've been successfully disconnected" }, 'success' );
            if(checkStorage) {
                localStorageService.remove("user");
            }
            if(checkCookie) {
                localStorageService.cookie.remove("user");
            }

            $location.path('/login');
        };
        logout(localStorageService, $location);

        $http.post('/sockets/remove', data);
    }]);
