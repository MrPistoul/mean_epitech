app.controller('adminUsersCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        var socket = io.connect();
        var loadData = function () {
            $http.put('/admin/users')
                .success(function(data, success) {
                    $rootScope.users = data.users;
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.admin == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.banishUser = function(id) {
            var data = { id: id };
            $http.post('/admin/banishUser', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.activateUser = function(id) {
            var data = { id: id };
            $http.post('/admin/activateUser', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);
