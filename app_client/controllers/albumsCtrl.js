app.controller('albumsCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', '$sce', 'Notification',
    function($rootScope, $scope, $http, $location, $route, $sce, Notification) {
        var socket = io.connect();
        var loadData = function () {
            var data = { owner: $rootScope.wall._id };
            $http.put('/albums', data)
                .success(function(data, success) {
                    data.albums.forEach(function(album) {
                        var isLiked = false;
                        album.like.forEach(function(userId) {
                            if (userId.userId == $rootScope.user._id) {
                                isLiked = true;
                            }
                        });
                        album.isLiked = isLiked;

                        var data = { id: album._id};
                        $http.put('/album', data)
                            .success(function(data, success) {
                                album.image = data.images[0];
                            })
                            .error(function(data) {
                                Notification({ message: "An error occurs..." }, 'error' );
                            });
                    });
                    $rootScope.albums = data.albums;

                    data = { owner: $rootScope.wall._id };
                    $http.put('/comments/albums', data)
                        .success(function(data, success) {
                            data.comments.forEach(function (comment) {
                                var isLiked = false;
                                comment.like.forEach(function (userId) {
                                    if (userId.userId == $rootScope.user._id) {
                                        isLiked = true;
                                    }
                                });
                                comment.isLiked = isLiked;
                            });
                            $rootScope.comments = data.comments;
                        })
                        .error(function(data) {
                            Notification({ message: "An error occurs..." }, 'error' );
                        });

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            if ($rootScope.wall == null) {
                $rootScope.wall = $rootScope.user;
            }
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.addAlbum = function(title, description) {
            var data = { title: title, description: description, owner: $rootScope.user };
            $http.post('/albums', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.title= '';
                    $scope.description= '';
                    // NOTIFY FRIENDS
                    var notification = { title: 'album message', message: $rootScope.user.name + ' has published a new album', type:'primary' };
                    $rootScope.user.friends.forEach(function(friend) {
                        var missive = { user: friend.user, notification: notification };
                        socket.emit('notification', missive);
                    });
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.commentateAlbum = function(id) {
            $rootScope.albumToComment = id;
            loadData();
        };

        $scope.likeAlbum = function(id) {
            var data = { id: id, userId: $rootScope.user._id };
            $http.post('/albums/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlikeAlbum = function(id) {
            var data = { id: id, userId: $rootScope.user._id };
            $http.post('/albums/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.postComment = function(id, commentContent, albumOwner) {
            var data = { content: commentContent, albumId: id, author: $rootScope.user._id, albumOwner: albumOwner };
            $http.post('/comments/album', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.commentContent= '';
                    $rootScope.albumToComment = '';
                    if ($rootScope.user._id != albumOwner) {
                        // notification to album owner
                        $http.put('/user', {id: albumOwner})
                            .success(function(data, status) {
                                var notification = { title: 'comment notification', message: $rootScope.user.name + ' has commented one of your albums', type:'primary' };
                                var missive = { user: data.user, notification: notification };
                                socket.emit('notification', missive);
                            });
                    }
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.removeComment = function(id) {
            var data = { id: id };
            $http.post('/comments/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.likeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlikeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.tagUserClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.tagTermClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.trustHtml = function(html) {
            return $sce.trustAsHtml(html);
        };

        $scope.clean = function() {
            $rootScope.searchText = '';
        };

        $scope.showAlbum = function(album) {
            $rootScope.albumToShow = album;
            $location.path('/album');
        };

        $scope.showMyAlbums = function() {
            $rootScope.wall = $rootScope.user;
            loadData();
        };

        $scope.sendPrivateMessage = function() {
            $rootScope.recipient = $rootScope.wall;
            $location.path('/messages/private');
        };
    }]);
