app.controller('ChatCtrl', ['$rootScope', '$scope', '$mdDialog', '$http', '$location', 'socket',
    function ($rootScope, $scope, $mdDialog, $http, $location, socket) {
        // var socket = io.connect();
        if ($rootScope.user != null) {
            $scope.messages = [];
            $scope.username = $rootScope.user.name;
        } else {
            $location.path('/login');
        }

        socket.on('message created', function (data) {
            $scope.messages.push(data);
            });

        $scope.StartChat = function (event) {
            var answer = $rootScope.user.name;
            $rootScope.room = $scope.room;
            socket.emit('new user', {
                username: answer,
                room: $scope.room
            });
            $http.put('/users/connectedFriends', { id: $rootScope.user._id })
                .success(function(data, status) {
                    data.users.forEach(function(user) {
                        var notification = { title: 'chat notification', message: $rootScope.user.name + ' has join ' + $rootScope.room + ' chat room', type:'primary' };
                        var missive = { user: user, notification: notification };
                        socket.emit('notification', missive);
                    });
                });
            var data = {room: $rootScope.room};
            $http.post('chat/messages', data).success(function (msgs) {
                $scope.messages = msgs;
            });
            $location.path('/chat');
        };

        $scope.send = function (msg) {
            socket.emit('new message', {
                room: $scope.room,
                message: msg,
                username: $rootScope.user.name,
                avatar: $rootScope.user.avatar
            });
    };
}])
    .directive('ngEnter', function () {
        return function ($scope, element, attrs) {
            element.bind("keypress", function (event) {
                if (event.which === 13) {
                    $scope.$apply(function () {
                        $scope.$eval(attrs.ngEnter);
                    });
                    event.preventDefault();
                    $scope.message = '';
                }
            });
        };
    })
    .factory('socket', function (socketFactory) {
        var myIoSocket = io.connect();
    
        var socket = socketFactory({
            ioSocket: myIoSocket
        });
        return socket;
    });
