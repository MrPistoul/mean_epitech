app.controller('wallCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', '$sce', 'Notification',
    function($rootScope, $scope, $http, $location, $route, $sce, Notification) {
        var socket = io.connect();
        var loadData = function() {
            var data = { wall: $rootScope.wall._id };
            $http.put('/posts', data)
                .success(function(data, success) {
                    data.posts.forEach(function(post) {
                        var isLiked = false;
                        post.like.forEach(function(userId) {
                            if (userId.userId == $rootScope.user._id) {
                                isLiked = true;
                            }
                        });
                        post.isLiked = isLiked;
                    });
                    $rootScope.posts = data.posts;

                    data = { wall: $rootScope.wall._id };
                    $http.put('/comments', data)
                        .success(function(data, success) {
                            data.comments.forEach(function (comment) {
                                var isLiked = false;
                                comment.like.forEach(function (userId) {
                                    if (userId.userId == $rootScope.user._id) {
                                        isLiked = true;
                                    }
                                });
                                comment.isLiked = isLiked;
                            });
                            $rootScope.comments = data.comments;
                        })
                        .error(function(data) {
                            Notification({ message: "An error occurs..." }, 'error' );
                        });

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            if ($rootScope.wall == null) {
                $rootScope.wall = $rootScope.user;
            }
            loadData();
        } else {
            $location.path('/login');
        }


        $scope.post = function() {
            var data = { content: $scope.content, author: $rootScope.user._id, wall: $rootScope.wall._id }; // link to add
            $http.post('/posts', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.content= '';
                    if ($rootScope.user._id != $rootScope.wall._id) {
                        // notification to wall owner
                        var notification = { title: 'post notification', message: $rootScope.user.name + ' has write a new post on your wall', type:'primary' };
                        var missive = { user: $rootScope.wall, notification: notification };
                        socket.emit('notification', missive);
                    }
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.edit = function(id) {
            $rootScope.postToUpdate = id;
            loadData();
        };

        $scope.commentate = function(id) {
            $rootScope.postToComment = id;
            //loadData();
        };

        $scope.update = function(id, newContent) {
            var data = { content: newContent, id: id };
            $http.post('/posts/update', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.newContent= '';
                    $rootScope.postToUpdate = '';
                    if ($rootScope.user._id != $rootScope.wall._id) {
                        // notification to wall owner
                        var notification = { title: 'post notification', message: $rootScope.user.name + ' has update his post on your wall', type:'primary' };
                        var missive = { user: $rootScope.wall, notification: notification };
                        socket.emit('notification', missive);
                    }
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.remove = function(id) {
            var data = { id: id };
            $http.post('/posts/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.like = function(postId) {
            var data = { postId: postId, userId: $rootScope.user._id };
            $http.post('/posts/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlike = function(postId) {
            var data = { postId: postId, userId: $rootScope.user._id };
            $http.post('/posts/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.postComment = function(id, commentContent, postAuthor) {
            var data = { content: commentContent, postId: id, author: $rootScope.user._id, postAuthor: postAuthor._id, wall: $rootScope.wall._id };
            $http.post('/comments', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.commentContent= '';
                    $rootScope.postToComment = '';
                    if ($rootScope.user._id != $rootScope.wall._id) {
                        // notification to wall owner
                        var notification = { title: 'comment notification', message: $rootScope.user.name + ' has commented a post on your wall', type:'primary' };
                        var missive = { user: $rootScope.wall, notification: notification };
                        socket.emit('notification', missive);
                    }
                    if ($rootScope.user._id != postAuthor._id) {
                        // notification to post owner
                        var notification = { title: 'comment notification', message: $rootScope.user.name + ' has commented your post on ' + $rootScope.wall.name + '\'s wall', type:'primary' };
                        var missive = { user: postAuthor, notification: notification };
                        socket.emit('notification', missive);
                    }
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.removeComment = function(id) {
            var data = { id: id };
            $http.post('/comments/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.likeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlikeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.showAlbums = function() {
            $location.path('/albums');
        };

        $scope.tagUserClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.tagTermClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.trustHtml = function(html) {
            return $sce.trustAsHtml(html);
        };

        $scope.clean = function() {
            $rootScope.searchText = '';
        };

        $scope.showMyWall = function() {
            $rootScope.wall = $rootScope.user;
            loadData();
        };

        $scope.sendPrivateMessage = function() {
            $rootScope.recipient = $rootScope.wall;
            $location.path('/messages/private');
        };
    }]);
