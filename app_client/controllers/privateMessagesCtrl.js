app.controller('privateMessageCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        var socket = io.connect();
        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
        } else {
            $location.path('/login');
        }
        $scope.addMessage = function(content) {
            var data = {
                from: $rootScope.user._id,
                to: $rootScope.recipient._id,
                content: content,
                type: 'private message'
            };
            $http.post('/messages', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    // NOTIFY THE APPLICANT
                    var notification = { title: 'private message', message: $rootScope.user.name + ' has sent you a private message', type:'primary' };
                    var missive = { user: $rootScope.recipient._id, notification: notification };
                    socket.emit('notification', missive);
                    history.back();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);
