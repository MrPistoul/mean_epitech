app.controller('profileCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Upload', '$timeout', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Upload, $timeout, Notification) {
        var socket = io.connect();
        var missive = { user: $rootScope.user };
        socket.emit('login', missive);
        if ($rootScope.user == null || $rootScope.user.status == false) {
            $location.path('/login');
        }

        // BADGE REFRESHING
        data = { id: $rootScope.user._id };
        $http.put('/messages', data)
            .success(function(data, status) {
                $rootScope.badge = data.messages.length;
            });

        $scope.updateProfile = function() {
            var data = {
                id: $rootScope.user._id,
                name: $scope.name,
                email: $scope.email,
                firstName: $scope.firstName,
                lastName: $scope.lastName,
                streetAddress: $scope.streetAddress,
                complementAddress: $scope.complementAddress,
                postalCode: $scope.postalCode,
                city: $scope.city,
                state: $scope.state,
                country: $scope.country };
            console.log(data);
            $http.post('/updateProfile', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $rootScope.user = data.user;
                    $scope.name= '';
                    $scope.email = '';
                    $scope.password = '';
                    $scope.conf_password = '';
                    $route.reload();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                    $scope.password = '';
                    $scope.conf_password = '';
                });
        };

        $scope.updatePassword = function() {
            var data = {
                id: $rootScope.user._id,
                oldPassword: $scope.oldPassword,
                newPassword: $scope.newPassword,
                conf_newPassword: $scope.conf_newPassword };
            $http.post('/updatePassword', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $rootScope.user = data.user;
                    $scope.oldPassword = '';
                    $scope.newPassword = '';
                    $scope.conf_newPassword = '';
                    $route.reload();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                    $scope.oldPassword = '';
                    $scope.newPassword = '';
                    $scope.conf_newPassword = '';
                });
        };

        $scope.unRegister = function() {
            var data = { id: $rootScope.user._id };
            $http.post('unRegister', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $rootScope.user = data.user;
                    $location.path('/logout');
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.uploadAvatar = function(file) {
            file.upload = Upload.upload({
                url: '/upload/avatar',
                method: 'POST',
                data: {userId: $rootScope.user._id},
                file: file
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    updateUser();
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        };

        updateUser = function() {
            var data = { id: $rootScope.user._id };
            $http.put('/user', data)
                .success(function (data, status) {
                    $rootScope.user = data.user;
                    Notification({ message: data.message }, 'success' );
                })
                .error(function (data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);
