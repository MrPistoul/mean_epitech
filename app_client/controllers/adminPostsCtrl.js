app.controller('adminPostsCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        var socket = io.connect();
        var loadData = function () {
            $http.put('/admin/posts')
                .success(function(data, success) {
                    $rootScope.posts = data.posts;
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.admin == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.banishPost = function(id) {
            var data = { id: id };
            $http.post('/admin/banishPost', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.activatePost = function(id) {
            var data = { id: id };
            $http.post('/admin/activatePost', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);
