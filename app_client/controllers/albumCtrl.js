app.controller('albumCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', '$sce', 'Upload', '$timeout', 'Notification',
    function($rootScope, $scope, $http, $location, $route, $sce, Upload, $timeout, Notification) {
        var socket = io.connect();
        var loadData = function () {
            var data = { id: $rootScope.albumToShow._id };
            $http.put('/album', data)
                .success(function(data, success) {
                    data.images.forEach(function(image) {
                        var isLiked = false;
                        image.like.forEach(function(userId) {
                            if (userId.userId == $rootScope.user._id) {
                                isLiked = true;
                            }
                        });
                        image.isLiked = isLiked;
                    });
                    $rootScope.images = data.images;

                    data = { album: $rootScope.albumToShow._id };
                    $http.put('/comments/album', data)
                        .success(function(data, success) {
                            data.comments.forEach(function (comment) {
                                var isLiked = false;
                                comment.like.forEach(function (userId) {
                                    if (userId.userId == $rootScope.user._id) {
                                        isLiked = true;
                                    }
                                });
                                comment.isLiked = isLiked;
                            });
                            $rootScope.comments = data.comments;
                        })
                        .error(function(data) {
                            Notification({ message: "An error occurs..." }, 'error' );
                        });

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            if ($rootScope.wall == null) {
                $rootScope.wall = $rootScope.user;
            }
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.updateAlbum = function(id, newTitle, newDescription) {
            var data = { title: newTitle, description: newDescription, id: id };
            $http.post('/albums/update', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.removeAlbum = function(id) {
            var data = { id: id };
            $http.post('/albums/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $location.path('/albums');
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.uploadImage = function(file, title) {
            file.upload = Upload.upload({
                url: '/upload/albums',
                method: 'POST',
                data: { file: file, user: $rootScope.user, album: $rootScope.albumToShow, title: title },
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    // NOTIFY FRIENDS
                    var notification = { title: 'album message', message: $rootScope.user.name + ' has published a new photo on ' + $rootScope.albumToShow.title + ' album', type:'primary' };
                    $rootScope.user.friends.forEach(function(friend) {
                        $http.put('/user', {id: friend.user})
                            .success(function(data, status) {
                                var missive = { user: data.user, notification: notification };
                                socket.emit('notification', missive);
                            });
                    });
                    loadData();
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        };

        $scope.removeImage = function(id) {
            var data = { id: id, albumId: $rootScope.albumToShow._id };
            $http.post('/images/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.commentateImage = function(id) {
            $rootScope.imageToComment = id;
            loadData();
        };

        $scope.openImage = function(path) {
            window.open(path, '_blank');
        };

        $scope.likeImage = function(id) {
            var data = { id: id, userId: $rootScope.user._id };
            $http.post('/images/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlikeImage = function(id) {
            var data = { id: id, userId: $rootScope.user._id };
            $http.post('/images/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.postComment = function(id, commentContent, imageOwner, albumId) {
            var data = { content: commentContent, imageId: id, author: $rootScope.user._id, imageOwner: imageOwner, albumId: albumId };
            $http.post('/comments/image', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.commentContent= '';
                    $rootScope.imageToComment = '';
                    if ($rootScope.user._id != imageOwner) {
                        // notification to image owner
                        $http.put('/user', {id: imageOwner})
                            .success(function(data, status) {
                                var notification = { title: 'comment notification', message: $rootScope.user.name + ' has commented one of your images', type:'primary' };
                                var missive = { user: data.user, notification: notification };
                                socket.emit('notification', missive);
                            });
                    }
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.removeComment = function(id) {
            var data = { id: id };
            $http.post('/comments/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.likeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlikeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.tagUserClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.tagTermClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.trustHtml = function(html) {
            return $sce.trustAsHtml(html);
        };

        $scope.clean = function() {
            $rootScope.searchText = '';
        };
    }]);
