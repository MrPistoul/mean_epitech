app.controller('messagesCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        $rootScope.recipient = null;
        var socket = io.connect();
        var loadData = function () {
            var data = { id : $rootScope.user._id};
            $http.put('/messages', data)
                .success(function(data, success) {
                    $rootScope.messages = data.messages;

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.addFriend = function(messageId, id) {
            var data = {
                id: $rootScope.user._id,
                applicant: id,
                messageId: messageId
            };
            $http.post('/users/addFriend', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    // NOTIFY THE APPLICANT
                    $http.put('/user', {id: id})
                        .success(function(data, status) {
                            var notification = { title: 'friendship response', message: $rootScope.user.name + ' agreed to be your friend', type:'primary' };
                            var missive = { user: data.user, notification: notification };
                            socket.emit('notification', missive);
                        });
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.rejectFriend = function(messageId, id) {
            var data = {
                id: $rootScope.user._id,
                applicant: id,
                messageId: messageId
            };
            $http.post('/users/rejectFriend', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    // NOTIFY THE APPLICANT
                    $http.put('/user', {id: id})
                        .success(function(data, status) {
                            var notification = { title: 'friendship response', message: $rootScope.user.name + ' don\'t want to be your friend', type:'warning' };
                            var missive = { user: data.user, notification: notification };
                            socket.emit('notification', missive);
                        });
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.readMessage = function(messageId) {
            var data = { id: messageId };
            $http.post('/messages/readMessage', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.replyMessage = function(messageId, user) {
            $rootScope.recipient = user;
            var data = { id: messageId };
            $http.post('/messages/readMessage', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $location.path('messages/private');
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);
