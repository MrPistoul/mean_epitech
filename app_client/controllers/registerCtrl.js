app.controller('registerCtrl', ['$rootScope', '$scope', '$http', '$location', 'Notification',
    function($rootScope, $scope, $http, $location, Notification) {
        $scope.register = function() {
            var data = { name: $scope.name, email: $scope.email, password: $scope.password, conf_password: $scope.conf_password };
            $http.post('/register', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.name= '';
                    $scope.email = '';
                    $scope.password = '';
                    $scope.conf_password = '';
                    $location.path('/login');
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                    $scope.password = '';
                    $scope.conf_password = '';
                })
        };
    }]);
