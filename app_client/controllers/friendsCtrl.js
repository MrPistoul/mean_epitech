app.controller('friendsCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        var socket = io.connect();
        var loadData = function () {
            var data = { id : $rootScope.user._id};
            $http.put('/users/friends', data)
                .success(function(data, success) {
                    data.users.forEach(function (user) {
                        user.friends.forEach(function (friend) {
                            if (friend.user && friend.user == $rootScope.user._id) {
                                user.friendshipDate = friend.createDate;
                            }
                        });
                    });
                    $rootScope.friends = data.users;

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.showFriend = function(user) {
            $rootScope.wall = user;
            $location.path('/wall');
        };

        $scope.removeFriend = function(id) {
            var data = {
                id: $rootScope.user._id,
                anonymous: id
            };
            $http.post('/users/removeFriend', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);
