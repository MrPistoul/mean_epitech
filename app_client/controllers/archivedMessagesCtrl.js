app.controller('archivedMessagesCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        $rootScope.recipient = null;
        var socket = io.connect();
        var loadData = function () {
            var data = { id : $rootScope.user._id};
            $http.put('/messages/old', data)
                .success(function(data, success) {
                    $rootScope.messages = data.messages;

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.replyMessage = function(messageId, user) {
            $rootScope.recipient = user;
            $location.path('messages/private');
        };
    }]);
