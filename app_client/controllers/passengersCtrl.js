app.controller('passengersCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        var socket = io.connect();
        var loadData = function () {
            var data = { id : $rootScope.user._id};
            $http.put('/users', data)
                .success(function(data, success) {
                    data.users.forEach(function(user) {
                        user.friends.forEach(function (friend) {
                            if (friend.user && friend.user == $rootScope.user._id && (friend.process == "accepted" || friend.process == "agreed")) {
                                user.friendship = true;
                            } else if (friend.user && friend.user == $rootScope.user._id && (friend.process == "request" || friend.process == "demand")) {
                                user.friendship = "in progress";
                            }
                        });
                    });
                    $rootScope.users = data.users;

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.requestFriend = function(id) {
            var data = { id: $rootScope.user._id, candidate: id };
            $http.post('/users/requestFriend', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    // NOTIFY THE CANDIDATE
                    $http.put('/user', {id: id})
                        .success(function(data, status) {
                            var notification = { title: 'friend request', message: $rootScope.user.name + ' want to be your friend', type:'primary' };
                            var missive = { user: data.user, notification: notification };
                            socket.emit('notification', missive);
                        });
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.showPassenger = function(id) {
            var data = { id: id };
            $http.put('/user', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $rootScope.passenger = data.user;
                    $location.path('/passenger');
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);
