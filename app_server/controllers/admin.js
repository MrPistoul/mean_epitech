var User = require('../models/users');
var Post = require('../models/posts');

var ctrlAdmin = {
    adminListUsers: function(req, res) {
        User.find(function(e, users) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.json({users: users});
            }
        })
    },

    adminBanishUser: function(req, res) {
        var date = new Date();
        User.findByIdAndUpdate(req.body.id, { $set: { status: false, deleteDate: date }}, function(e, user) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "The user has been banished."});
            }
        })
    },
    
    adminActivateUser: function(req, res) {
        var date = new Date();
        User.findByIdAndUpdate(req.body.id, { $set: { status: true, deleteDate: "", updateDate: date }}, function(e, user) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "The user has been activated again."});
            }
        })
    },

    adminListPosts: function(req, res) {
        Post.aggregate([
            { $lookup: {
                from: "users",
                localField: "author",
                foreignField: "_id",
                as: "authorDatas"}},
            { $sort: {updateDate: -1}}
        ], function(e, posts) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({posts: posts});
            }
        });
    },

    adminBanishPost: function(req, res) {
        var date = new Date();
        Post.findByIdAndUpdate(req.body.id, { $set: { status: false, deleteDate: date }}, function(e, post) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: " The post has been banished."});
            }
        })
    },

    adminActivatePost: function(req, res) {
        var date = new Date();
        Post.findByIdAndUpdate(req.body.id, { $set: { status: true, deleteDate: date, updateDate: date }}, function(e, post) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "The post has been activated again."});
            }
        })
    }
};

module.exports = ctrlAdmin;