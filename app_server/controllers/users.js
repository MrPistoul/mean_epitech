var User = require('../models/users');
var Message = require('../models/messages');
var Socket = require('../models/sockets');
var mongoose = require('mongoose');

var ctrlUsers = {
    usersReadOne: function(req, res) {
        User.findById(req.body.id, function(e, user) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.json({user: user});
            }
        });
    },
    
    usersReadAll: function(req, res) {
        User.find({ status: true, _id: { $ne: req.body.id }}).sort({name: 1}).exec(function(e, users) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.json({users: users});
            }
        });
    },

    usersListFriends: function(req, res) {
        User.find({ status: true, _id: { $ne: req.body.id }, friends: {$elemMatch: { user: req.body.id, status: true }}}).sort({name: 1}).exec(function (e, users) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({users: users});
            }
        });
    },

    usersListConnectedFriends: function(req, res) {
        User.find({ status: true, _id: { $ne: req.body.id }, friends: {$elemMatch: { user: req.body.id, status: true }}}).sort({name: 1}).exec(function (e, users) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                var connectedUsers = [];
                Socket.find(function (e, sockets) {
                    if (e) {
                        res.status(400).json({message: "An error occurs"});
                    } else {
                        var sckts = sockets.map(function(socket) {
                            return socket.userId.toString();
                        });
                        users.forEach(function(user) {
                            if (sckts.indexOf(user._id.toString()) != -1) {
                                connectedUsers.push(user);
                            }
                        });
                        res.status(200).json({users: connectedUsers});
                    }
                });
            }
        });
    },

    usersRequestFriend: function(req, res) {
        User.findByIdAndUpdate(req.body.id, { $addToSet: { friends: { user: req.body.candidate, status: false, process: "request" }}}, function(e, user) {
            User.findByIdAndUpdate(req.body.candidate, { $addToSet: { friends: { user: req.body.id, status: false, process: "demand" }}}, function(e, user) {
                if (e) {
                    res.status(400).json({message: "An error occurs"});
                } else {
                    var newMessage = new Message({
                        content: "Would you be my friend ?",
                        type: 'friend request',
                        author: req.body.id,
                        recipient: req.body.candidate
                    });
                    newMessage.save();
                    res.json({message: "Your request has been sent"});
                }
            });
        });
    },

    usersAddFriend: function(req, res) {
        var date = new Date();
        var id = new mongoose.Types.ObjectId(req.body.id);
        var applicant = new mongoose.Types.ObjectId(req.body.applicant);
        User.update({ _id: id, friends: { $elemMatch: { user: applicant }}}, { $set: { 'friends.$.status': true, 'friends.$.process': "accepted", 'friends.$.updateDate': date }}, function(e, user) {
            User.update({ _id: applicant, friends: { $elemMatch: { user: id }}}, { $set: { 'friends.$.status': true, 'friends.$.process': "agreed", 'friends.$.updateDate': date }}, function(e, user) {
                if (e) {
                    res.status(400).json({message: "An error occurs"});
                } else {
                    var newMessage = new Message({
                        content: "Yes I want to be your friend !",
                        type: 'friendship response',
                        author: req.body.id,
                        recipient: req.body.applicant
                    });
                    newMessage.save();
                    Message.findByIdAndUpdate(req.body.messageId, { $set: {status: false, deleteDate: date}}, function (e, message) {
                        if (e) {
                            res.status(400).json({message: "An error occurs with message"});
                        } else {
                            res.json({message: "Your friendship is sealed"});
                        }
                    });
                }
            });
        });
    },

    usersRejectFriend: function(req, res) {
        var date = new Date();
        User.findByIdAndUpdate(req.body.id, {$pull: {friends: {user: req.body.applicant}}}, function (e, user) {
            User.findByIdAndUpdate(req.body.applicant, {$pull: {friends: {user: req.body.id}}}, function (e, user) {
                User.findByIdAndUpdate(req.body.id, {
                    $addToSet: {
                        archivedFriends: {
                            user: req.body.applicant,
                            process: "dismissed",
                            deleteDate: date
                        }
                    }
                }, function (e, user) {
                    User.findByIdAndUpdate(req.body.applicant, {
                        $addToSet: {
                            archivedFriends: {
                                user: req.body.id,
                                process: "rejected",
                                deleteDate: date
                            }
                        }
                    }, function (e, user) {
                        if (e) {
                            res.status(400).json({message: "An error occurs"});
                        } else {
                            var newMessage = new Message({
                                content: "No, I don't want to be your friend.",
                                type: 'friendship response',
                                author: req.body.id,
                                recipient: req.body.applicant
                            });
                            newMessage.save();
                            Message.findByIdAndUpdate(req.body.messageId, {
                                $set: {
                                    status: false,
                                    deleteDate: date
                                }
                            }, function (e, message) {
                                if (e) {
                                    res.status(400).json({message: "An error occurs with message"});
                                } else {
                                    res.json({message: "The request has been dismissed"});
                                }
                            });
                        }
                    });
                });
            });
        });
    },

    usersRemoveFriend: function(req, res) {
        var date = new Date();
        User.findByIdAndUpdate(req.body.id, {$pull: {friends: {user: req.body.anonymous}}}, function (e, user) {
            User.findByIdAndUpdate(req.body.anonymous, {$pull: {friends: {user: req.body.id}}}, function (e, user) {
                User.findByIdAndUpdate(req.body.id, {
                    $addToSet: {
                        archivedFriends: {
                            user: req.body.anonymous,
                            process: "removed",
                            deleteDate: date
                        }
                    }
                }, function (e, user) {
                    User.findByIdAndUpdate(req.body.anonymous, {
                        $addToSet: {
                            archivedFriends: {
                                user: req.body.id,
                                process: "cancelled",
                                deleteDate: date
                            }
                        }
                    }, function (e, user) {
                        if (e) {
                            res.status(400).json({message: "An error occurs"});
                        } else {
                            res.json({message: "Your friendship is an old idea"});
                        }
                    });
                });
            });
        });
    }
};

module.exports = ctrlUsers;
