var Post = require('../models/posts');
var User = require('../models/users');
var Message = require('../models/messages');
var mongoose = require('mongoose');

var ctrlPosts = {
    postsReadAll: function(req, res) {
        var id = new mongoose.Types.ObjectId(req.body.wall);
        Post.aggregate([
            { $match: { status: true, wall: id }},
            { $lookup: {
                from: "users",
                localField: "author",
                foreignField: "_id",
                as: "authorDatas"}},
            { $sort: {createDate: -1}}
        ], function(e, posts) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({posts: posts});
            }
        });
    },

    postsAddOne: function(req, res) {
        User.find(function (e, users) {
            var ids = users.map(function (user) {
                return user._id;
            });
            if (req.body.content && req.body.author && req.body.wall) {
                var check = true;
                var message = '';

                if (ids.indexOf(req.body.author) != -1) {
                    check = false;
                    message += 'Unknown author, what are you doing ? ';
                }
                if (ids.indexOf(req.body.wall) != -1) {
                    check = false;
                    message += 'Unknown wall, what are you doing ? ';
                }
                if (req.body.content.length < 20) {
                    check = false;
                    message += 'Please be a little bit more expressive. ';
                }
                if (req.body.content.length > 140) {
                    check = false;
                    message += 'Your post is too long ! Please be synthetic. ';
                }

                if (check) {
                    var newPost = new Post({
                        content: req.body.content,
                        author: req.body.author,
                        wall: req.body.wall
                    });
                    newPost.save(function (e) {
                        if (e) {
                            res.status(400).json({message: "An error occurs"});
                        } else {
                            if (req.body.author != req.body.wall) {
                                var newMessage = new Message({
                                    content: "I have post a new message on your wall",
                                    type: 'post message',
                                    author: req.body.author,
                                    recipient: req.body.wall
                                });
                                newMessage.save();
                            }
                            res.status(200).json({message: "Post added"});
                        }
                    });
                } else {
                    res.status(404).json({message: message});
                }
            } else {
                res.status(400).json({message: 'Please type all fields to post'});
            }
        });
    },
    
    postsUpdateOne: function(req, res) {
        if (req.body.content && req.body.id) {
            var check = true;
            var message = '';

            if (req.body.content.length < 20) {
                check = false;
                message += 'Please be a little bit more expressive. ';
            }
            if (req.body.content.length > 140) {
                check = false;
                message += 'Your post is too long ! Please be synthetic. ';
            }

            if (check) {
                var date = new Date();
                Post.findByIdAndUpdate(req.body.id, { $set: { content: req.body.content, updateDate: date }}, function(e, post) {
                    if (e) {
                        res.status(400).json({message: "An error occurs"});
                    } else {
                        res.status(200).json({message: "Your post has been updated !"});
                    }
                });
            } else {
                res.status(404).json({message: message});
            }
        } else {
                res.status(400).json({message: 'Please type all fields to post'});
            }

    },

    postsAddLike: function(req, res) {
        Post.findByIdAndUpdate(req.body.postId, { $addToSet: { like: { userId: req.body.userId }}}, function(e, post) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "Your like is registered !"});
            }
        });
    },

    postsRemoveLike: function(req, res) {
        Post.findByIdAndUpdate(req.body.postId, { $pull: { like: { userId: req.body.userId }}}, function(e, post) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "Your unlike is registered..."});
            }
        });
    },

    postsDeleteOne: function(req, res) {
        var date = new Date();
            Post.findByIdAndUpdate(req.body.id, { $set: { status: false, deleteDate: date }}, function(e, post) {
                if (e) {
                    res.status(400).json({message: "An error occurs"});
                } else {
                    res.status(200).json({message: "Your post has been deleted !"});
                }
            });
    }
};

module.exports = ctrlPosts;
