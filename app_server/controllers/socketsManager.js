var Socket = require('../models/sockets');
var Chat = require('../models/chat');
var mongoose = require('mongoose');

module.exports = {
    start: function(io) {
        io.set('heartbeat timeout', 5);
        io.on('connection', function(socket) {
            socket.on('login', function(data) {
                Socket.findOne({userId: data.user._id}, function (e, sock) {
                    if (sock == null) {
                        var newSocket = new Socket({
                            socketId: socket.id,
                            userId: data.user._id
                        });
                        newSocket.save();
                    }
                });
            });
            
            socket.on('notification', function(data) {
                Socket.findOne({ userId: data.user._id }, function(e, sock) {
                    if (e) {
                        res.status(400).json({message: "An error occurs during notification"});
                    } else {
                        if (sock != null) {
                            io.sockets.clients(function (e, clients) {
                                if (clients.indexOf(sock.socketId) != -1) {
                                    socket.broadcast.to(sock.socketId).emit('notification', data.notification);
                                } else {
                                    sock.remove();
                                }
                            });
                        }
                    }
                });
            });
        });
    },
    chat: function (io) {
        //Listen for connection
        io.on('connection', function (socket) {
            
            //Listens for new user
            socket.on('new user', function (data) {
                //New user joins the default room
                socket.join(data.room);
            });

            //Listens for a new chat message
            socket.on('new message', function (data) {
                //Create message
                var newMsg = new Chat({
                    username: data.username,
                    content: data.message,
                    avatar: data.avatar,
                    room: data.room.toLowerCase(),
                    created: new Date(),

                });
                //Save it to database
                newMsg.save(function (err, msg) {
                    //Send message to those connected in the room
                    io.emit('message created', msg);
                });
            });
        });
    },
};
