var Socket = require('../models/sockets');

var ctrlSockets = {
    socketsFindOne: function(req, res) {
        Socket.findOne({ userId: req.body.userId}, function(e, socket) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.json({socket: socket});
            }
        });
    },

    socketsRemoveOne: function(req, res) {
        Socket.remove({ userId: req.body.userId}, function(e, socket) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            }
        });
    }
};

module.exports = ctrlSockets;
