var Album = require('../models/albums');
var Post = require('../models/posts');
var User = require('../models/users');
var Message = require('../models/messages');
var mongoose = require('mongoose');

var ctrlAlbums = {
    albumsListAll: function(req, res) {
        Album.find({status: true, owner: req.body.owner}).sort({createDate: -1}).exec(
        function(e, albums) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({albums: albums});
            }
        });
    },

    albumsAddOne: function(req, res) {
        User.find(function (e, users) {
            var ids = users.map(function (user) {
                return user._id;
            });
            if (req.body.title && req.body.description && req.body.owner) {
                var check = true;
                var message = '';

                if (ids.indexOf(req.body.owner._id) != -1) {
                    check = false;
                    message += 'Unknown owner, what are you doing ? ';
                }
                if (req.body.title.length < 5 || req.body.title.length > 50) {
                    check = false;
                    message += 'Please type a valid title (between 5 and 50 characters). ';
                }
                if (req.body.description.length > 140) {
                    check = false;
                    message += 'Your description is too long ! Please be synthetic. ';
                }

                if (check) {
                    var newAlbum = new Album({
                        title: req.body.title,
                        description: req.body.description,
                        owner: req.body.owner._id
                    });
                    newAlbum.save(function (e) {
                        if (e) {
                            res.status(400).json({message: "An error occurs"});
                        } else {
                            req.body.owner.friends.forEach(function (friend) {
                                if (friend.status) {
                                    var newMessage = new Message({
                                        content: "I have created a new album titled " + req.body.title,
                                        type: 'album message',
                                        author: req.body.owner._id,
                                        recipient: friend.user
                                    });
                                    newMessage.save();
                                }
                            });
                            res.status(200).json({message: "Album added"});
                        }
                    });
                } else {
                    res.status(404).json({message: message});
                }
            } else {
                res.status(400).json({message: 'Please type all fields to add an album'});
            }
        });
    },
    
    albumsUpdateOne: function(req, res) {
        if (req.body.title && req.body.description && req.body.id) {
            var check = true;
            var message = '';
            
            if (req.body.title.length < 5 || req.body.title.length > 50) {
                check = false;
                message += 'Please type a valid title (between 5 and 50 characters). ';
            }
            if (req.body.description.length > 140) {
                check = false;
                message += 'Your description is too long ! Please be synthetic. ';
            }

            if (check) {
                var date = new Date();
                Album.findByIdAndUpdate(req.body.id, { $set: { title: req.body.title, description: req.body.description, updateDate: date }}, function(e, post) {
                    if (e) {
                        res.status(400).json({message: "An error occurs"});
                    } else {
                        res.status(200).json({message: "Your album has been updated !"});
                    }
                });
            } else {
                res.status(404).json({message: message});
            }
        } else {
                res.status(400).json({message: 'Please type all fields to update an album'});
            }

    },

    albumsAddLike: function(req, res) {
        Album.findByIdAndUpdate(req.body.id, { $addToSet: { like: { userId: req.body.userId }}}, function(e, post) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "Your like is registered !"});
            }
        });
    },

    albumsRemoveLike: function(req, res) {
        Album.findByIdAndUpdate(req.body.id, { $pull: { like: { userId: req.body.userId }}}, function(e, post) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "Your unlike is registered..."});
            }
        });
    },

    albumsDeleteOne: function(req, res) {
        var date = new Date();
            Album.findByIdAndUpdate(req.body.id, { $set: { status: false, deleteDate: date }}, function(e, post) {
                if (e) {
                    res.status(400).json({message: "An error occurs"});
                } else {
                    res.status(200).json({message: "Your album has been deleted !"});
                }
            });
    }
};

module.exports = ctrlAlbums;
