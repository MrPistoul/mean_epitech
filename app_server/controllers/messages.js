var Message = require('../models/messages');
var User = require('../models/users');
var mongoose = require('mongoose');

var ctrlMessages = {
    messagesListAllActive: function(req, res) {
        var id = new mongoose.Types.ObjectId(req.body.id);
        Message.aggregate([
            { $match: { status: true, recipient: id }},
            { $lookup: {
                from: "users",
                localField: "author",
                foreignField: "_id",
                as: "authorDatas"}},
            { $sort: {createDate: -1}}
        ], function(e, messages) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({messages: messages});
            }
        });
    },

    messagesListAllArchived: function(req, res) {
        var id = new mongoose.Types.ObjectId(req.body.id);
        Message.aggregate([
            { $match: { status: false, recipient: id }},
            { $lookup: {
                from: "users",
                localField: "author",
                foreignField: "_id",
                as: "authorDatas"}},
            { $sort: {createDate: -1}}
        ], function(e, messages) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({messages: messages});
            }
        });
    },

    messagesAddOne: function(req, res) {
        User.find(function (e, users) {
            var ids = users.map(function (user) {
                return user._id;
            });
            if (req.body.content && req.body.to && req.body.from && req.body.type) {
                var check = true;
                var message = '';

                if (ids.indexOf(req.body.to) != -1) {
                    check = false;
                    message += 'Unknown author, what are you doing ? ';
                }
                if (ids.indexOf(req.body.from) != -1) {
                    check = false;
                    message += 'Unknown recipient, what are you doing ? ';
                }
                if (req.body.content.length < 10) {
                    check = false;
                    message += 'Please be a little bit more expressive. ';
                }
                if (req.body.content.length > 255) {
                    check = false;
                    message += 'Your post is too long ! Please be synthetic. ';
                }

                if (check) {
                    var newMessage = new Message({
                        content: req.body.content,
                        type: req.body.type,
                        author: req.body.from,
                        recipient: req.body.to
                    });
                    newMessage.save(function (e) {
                        if (e) {
                            res.status(400).json({message: "An error occurs"});
                        } else {
                            res.status(200).json({message: "Message posted"});
                        }
                    });
                } else {
                    res.status(404).json({message: message});
                }
            } else {
                res.status(400).json({message: 'Please type all fields to send a message'});
            }
        });
    },

    messagesReadOne: function(req, res) {
        var date = new Date();
        Message.findByIdAndUpdate(req.body.id, { $set: { status: false, deleteDate: date }}, function(e, post) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "The message has been archived !"});
            }
        });
    }
};

module.exports = ctrlMessages;
