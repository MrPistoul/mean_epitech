var Post = require('../models/posts');
var User = require('../models/users');
var Album = require('../models/albums');
var Image = require('../models/images');
var Comment = require('../models/comments');
var Message = require('../models/messages');
var mongoose = require('mongoose');

var ctrlComments = {
    commentsReadAll: function(req, res) {
        var id = new mongoose.Types.ObjectId(req.body.wall);
        Comment.aggregate([
            { $match: { status: true, wall: id }},
            { $lookup: {
                from: "users",
                localField: "author",
                foreignField: "_id",
                as: "authorDatas"}},
            { $sort: {createDate: -1}}
        ], function(e, comments) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({comments: comments});
            }
        });
    },

    commentsReadAllOnAlbums: function(req, res) {
        var id = new mongoose.Types.ObjectId(req.body.owner);
        Comment.aggregate([
            { $match: { status: true, owner: id }},
            { $lookup: {
                from: "users",
                localField: "author",
                foreignField: "_id",
                as: "authorDatas"}},
            { $sort: {createDate: -1}}
        ], function(e, comments) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({comments: comments});
            }
        });
    },

    commentsReadAllForAnAlbum: function(req, res) {
        var id = new mongoose.Types.ObjectId(req.body.album);
        Comment.aggregate([
            { $match: { status: true, album: id }},
            { $lookup: {
                from: "users",
                localField: "author",
                foreignField: "_id",
                as: "authorDatas"}},
            { $sort: {createDate: -1}}
        ], function(e, comments) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({comments: comments});
            }
        });
    },
    
    commentsAddOne: function(req, res) {
        User.find(function (e, users) {
            var userIds = users.map(function (user) {
                return user._id;
            });
            var postIds = users.map(function (user) {
                return user._id;
            });
            if (req.body.content && req.body.author && req.body.postId) {
                var check = true;
                var message = '';

                if (userIds.indexOf(req.body.author) != -1) {
                    check = false;
                    message += 'Unknown author, what are you doing ? ';
                }
                if (postIds.indexOf(req.body.postId) != -1) {
                    check = false;
                    message += 'Unknown post, what are you doing ? ';
                }
                if (req.body.content.length < 5) {
                    check = false;
                    message += 'Please be a little bit more expressive. ';
                }
                if (req.body.content.length > 140) {
                    check = false;
                    message += 'Your post is too long ! Please be synthetic. ';
                }

                if (check) {
                    var newComment = new Comment({
                        content: req.body.content,
                        author: req.body.author,
                        post: req.body.postId,
                        wall: req.body.wall
                    });
                    newComment.save(function (e) {
                        if (e) {
                            res.status(400).json({message: "An error occurs"});
                        } else {
                            Post.findByIdAndUpdate(req.body.postId, { $addToSet: { comments: { commentId: newComment._id }}}, function(e, post) {
                                if (e) {
                                    res.status(400).json({message: "An error occurs"});
                                } else {
                                    if (req.body.author != req.body.postAuthor) {
                                        var newMessage = new Message({
                                            content: "I have post a new comment on your post",
                                            type: 'post message',
                                            author: req.body.author,
                                            recipient: req.body.postAuthor
                                        });
                                        newMessage.save();
                                    }
                                    if (req.body.author != req.body.wall && req.body.wall != req.body.postAuthor) {
                                        var anotherMessage = new Message({
                                            content: "I have post a new comment on a post on your wall",
                                            type: 'post message',
                                            author: req.body.author,
                                            recipient: req.body.wall
                                        });
                                        anotherMessage.save();
                                    }
                                    res.status(200).json({message: "Comment added"});
                                }
                            });
                        }
                    });
                } else {
                    res.status(404).json({message: message});
                }
            } else {
                res.status(400).json({message: 'Please type all fields to post'});
            }
        });
    },

    commentsAddOneOnAlbum: function(req, res) {
        User.find(function (e, users) {
            var userIds = users.map(function (user) {
                return user._id;
            });
            if (req.body.content && req.body.author && req.body.albumId && req.body.albumOwner) {
                var check = true;
                var message = '';

                if (userIds.indexOf(req.body.author) != -1) {
                    check = false;
                    message += 'Unknown author, what are you doing ? ';
                }
                if (req.body.content.length < 5) {
                    check = false;
                    message += 'Please be a little bit more expressive. ';
                }
                if (req.body.content.length > 140) {
                    check = false;
                    message += 'Your post is too long ! Please be synthetic. ';
                }

                if (check) {
                    var newComment = new Comment({
                        content: req.body.content,
                        author: req.body.author,
                        album: req.body.albumId,
                        owner: req.body.albumOwner
                    });
                    newComment.save(function (e) {
                        if (e) {
                            res.status(400).json({message: "An error occurs"});
                        } else {
                            Album.findByIdAndUpdate(req.body.albumId, { $addToSet: { comments: { commentId: newComment._id }}}, function(e, post) {
                                if (e) {
                                    res.status(400).json({message: "An error occurs"});
                                } else {
                                    if (req.body.author != req.body.albumOwner) {
                                        var newMessage = new Message({
                                            content: "I have post a new comment on your album",
                                            type: 'album message',
                                            author: req.body.author,
                                            recipient: req.body.albumOwner
                                        });
                                        newMessage.save();
                                    }
                                    res.status(200).json({message: "Comment added"});
                                }
                            });
                        }
                    });
                } else {
                    res.status(404).json({message: message});
                }
            } else {
                res.status(400).json({message: 'Please type all fields to comment'});
            }
        });
    },

    commentsAddOneOnImage: function(req, res) {
        User.find(function (e, users) {
            var userIds = users.map(function (user) {
                return user._id;
            });
            if (req.body.content && req.body.author && req.body.albumId && req.body.imageId && req.body.imageOwner) {
                var check = true;
                var message = '';

                if (userIds.indexOf(req.body.author) != -1) {
                    check = false;
                    message += 'Unknown author, what are you doing ? ';
                }
                if (req.body.content.length < 5) {
                    check = false;
                    message += 'Please be a little bit more expressive. ';
                }
                if (req.body.content.length > 140) {
                    check = false;
                    message += 'Your post is too long ! Please be synthetic. ';
                }

                if (check) {
                    var newComment = new Comment({
                        content: req.body.content,
                        author: req.body.author,
                        album: req.body.albumId,
                        image: req.body.imageId,
                        owner: req.body.imageOwner
                    });
                    newComment.save(function (e) {
                        if (e) {
                            res.status(400).json({message: "An error occurs"});
                        } else {
                            Image.findByIdAndUpdate(req.body.imageId, { $addToSet: { comments: { commentId: newComment._id }}}, function(e, post) {
                                if (e) {
                                    res.status(400).json({message: "An error occurs"});
                                } else {
                                    if (req.body.author != req.body.imageOwner) {
                                        var newMessage = new Message({
                                            content: "I have post a new comment on an image",
                                            type: 'album message',
                                            author: req.body.author,
                                            recipient: req.body.imageOwner
                                        });
                                        newMessage.save();
                                    }
                                    res.status(200).json({message: "Comment added"});
                                }
                            });
                        }
                    });
                } else {
                    res.status(404).json({message: message});
                }
            } else {
                res.status(400).json({message: 'Please type all fields to post'});
            }
        });
    },

    commentsAddLike: function(req, res) {
        Comment.findByIdAndUpdate(req.body.commentId, { $addToSet: { like: { userId: req.body.userId }}}, function(e, post) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "Your like is registered !"});
            }
        });
    },

    commentsRemoveLike: function(req, res) {
        Comment.findByIdAndUpdate(req.body.commentId, { $pull: { like: { userId: req.body.userId }}}, function(e, post) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "Your unlike is registered..."});
            }
        });
    },

    commentsDeleteOne: function(req, res) {
        var date = new Date();
            Comment.findByIdAndUpdate(req.body.id, { $set: { status: false, deleteDate: date }}, function(e, post) {
                if (e) {
                    res.status(400).json({message: "An error occurs"});
                } else {
                    res.status(200).json({message: "Your comment has been deleted !"});
                }
            });
    }
};

module.exports = ctrlComments;
