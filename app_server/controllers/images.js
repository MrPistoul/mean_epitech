var Album = require('../models/albums');
var Image = require('../models/images');
var Post = require('../models/posts');
var User = require('../models/users');
var mv = require('mv');
var mkdirp = require('mkdirp');
var path = require('path');
var Message = require('../models/messages');

var ctrlImages = {
    imagesShowOneAlbum: function(req, res) {
        Image.find({status: true, album: req.body.id}).sort({createDate: -1}).exec(
            function(e, images) {
                if (e) {
                    res.status(400).json({message: "An error occurs"});
                } else {
                    res.status(200).json({images: images});
                }
        });
    },

    imagesAddOne: function(req, res) {
        var file = req.files.file;
        var newDir = '/upload/albums/' + req.body.album._id;
        var newPath = newDir + '/' + req.body.title + '.' + file.type.split('/')[1];
        mkdirp(path.join(__dirname, '/../../public/' + newDir), function (err) {
            if (err) {
                res.status(400).json({message: "An error occurs when creating dir"});
            } else {
                mv(file.path, __dirname + '/../../public/' + newPath, function (e) {
                    if (e) {
                        res.status(400).json({message: "An error occurs when saving"});
                    } else {
                        var newImage = new Image({
                            title: req.body.title,
                            album: req.body.album._id,
                            owner: req.body.user._id,
                            path: newPath
                        });
                        newImage.save(function (e, image) {
                            if (e) {
                                res.status(400).json({message: "An error occurs"});
                            } else {
                                if (req.body.user.friends) {
                                    req.body.user.friends.forEach(function (friend) {
                                        if (friend.status) {
                                            var newMessage = new Message({
                                                content: "I have upload a new image on my album " + req.body.album.title,
                                                type: 'album message',
                                                author: req.body.user._id,
                                                recipient: friend.user
                                            });
                                            newMessage.save();
                                        }
                                    });
                                }
                                Album.findByIdAndUpdate(req.body.album._id, { $addToSet: { images: { image: image._id}}}, function (e, album) {
                                    if (e) {
                                        res.status(400).json({message: "An error occurs"});
                                    } else {
                                        res.json({message: "Your image has been uploaded"});
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    },

    imagesAddLike: function(req, res) {
        Image.findByIdAndUpdate(req.body.id, { $addToSet: { like: { userId: req.body.userId }}}, function(e, post) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "Your like is registered !"});
            }
        });
    },

    imagesRemoveLike: function(req, res) {
        Image.findByIdAndUpdate(req.body.id, { $pull: { like: { userId: req.body.userId }}}, function(e, post) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.status(200).json({message: "Your unlike is registered..."});
            }
        });
    },

    imagesDeleteOne: function(req, res) {
        var date = new Date();
            Image.findByIdAndUpdate(req.body.id, { $set: { status: false, deleteDate: date }}, function(e, post) {
                if (e) {
                    res.status(400).json({message: "An error occurs"});
                } else {
                    Album.findByIdAndUpdate(req.body.albumId, { $pull: { images: { image: req.body.id}}}, function (e, album) {
                        if (e) {
                            res.status(400).json({message: "An error occurs"});
                        } else {
                            res.status(200).json({message: "Your image has been deleted !"});
                        }
                    });
                }
            });
    }
};

module.exports = ctrlImages;
