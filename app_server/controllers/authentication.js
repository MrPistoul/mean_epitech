var User = require('../models/users');
var bcrypt = require('bcryptjs');
var mv = require('mv');
var salt = bcrypt.genSaltSync(12);

var ctrlAuth = {
    register: function(req, res) {
        User.find(function(e, users) {
            var names = users.map(function(user) {
                return user.name;
            });
            var emails = users.map(function(user) {
                return user.email;
            });
            if (req.body.name && req.body.email && req.body.password && req.body.conf_password) {
                var check = true;
                var testName = new RegExp(/^[a-zA-Z0-9_-]+$/);
                var testEmail = new RegExp(/^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/);
                var message = '';

                if (!testName.test(req.body.name)) {
                    check = false;
                    message += 'Invalid name. ';
                }
                if (names.indexOf(req.body.name) != -1) {
                    check = false;
                    message += 'Name already exists. ';
                }
                if (!testEmail.test(req.body.email)) {
                    check = false;
                    message += 'Invalid email. ';
                }
                if (emails.indexOf(req.body.email) != -1) {
                    check = false;
                    message += 'Email already exists. ';
                }
                if (req.body.password != req.body.conf_password || req.body.password.length < 8) {
                    check = false;
                    message += 'Invalid confirmation password or length given (8 characters needed). ';
                }

                if (check) {
                    var newUser = new User({
                        name: req.body.name,
                        email: req.body.email,
                        password: bcrypt.hashSync(req.body.password, salt)
                    });
                    newUser.save();
                    res.status(200).json({message: "You're profile has been created", user: newUser});
                } else {
                    res.status(404).json({message: message});
                }
            } else {
                res.status(400).json({message: 'Please type all fields to register'});
            }
        });
    },

    login: function(req, res) {
        if (req.body.login && req.body.password) {
            User.findOne({ $or: [ { email: req.body.login, status: true }, { name: req.body.login, status: true }] }, function(e, user) {
                if (user) {
                    if (bcrypt.compareSync(req.body.password, user.password)) {
                        res.status(200).json({message: 'Login with success', user: user});
                    } else {
                        res.status(404).json({message: 'Wrong login/password'});
                    }
                } else {
                    res.status(404).json({message: 'Wrong login/password'});
                }
            });
        } else {
            res.status(400).json({message: 'All fields are required for login'});
        }
    },

    updateProfile: function(req, res) {
        User.find({ _id: { $ne: req.body.id }}, function(e, users) {
            var names = users.map(function(user) {
                return user.name;
            });
            var emails = users.map(function(user) {
                return user.email;
            });
            if (req.body.name && req.body.email) {
                var check = true;
                var testName = new RegExp(/^[a-zA-Z0-9_-]+$/);
                var testEmail = new RegExp(/^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/);
                var message = '';

                if (!testName.test(req.body.name)) {
                    check = false;
                    message += 'Invalid name. ';
                }
                if (names.indexOf(req.body.name) != -1) {
                    check = false;
                    message += 'Name already exists. ';
                }
                if (!testEmail.test(req.body.email)) {
                    check = false;
                    message += 'Invalid email. ';
                }
                if (emails.indexOf(req.body.email) != -1) {
                    check = false;
                    message += 'Email already exists. ';
                }
                if (req.body.firstName != null && req.body.firstName.length > 0) {
                    if (req.body.lastName == null) {
                        message += 'First and last name are required if one is fulfilled. ';
                        check = false;
                    } else if (req.body.firstName.length < 1 || req.body.firstName.length > 50) {
                        message += 'First name length is invalid. ';
                        check = false;
                    }
                }
                if (req.body.lastName != null && req.body.lastName.length > 0) {
                    if (req.body.firstName == null) {
                        message += 'First and last name are required if one is fulfilled ';
                        check = false;
                    } else if (req.body.lastName.length < 1 || req.body.lastName.length > 50) {
                        message += 'Last name length is invalid. ';
                        check = false;
                    }
                }
                if ((req.body.streetAddress != null || req.body.postalCode != null || req.body.city != null || req.body.country != null) && (req.body.streetAddress.length != 0 || req.body.postalCode.length != 0 || req.body.country.length != 0 || req.body.city.length != 0)) {
                    if (req.body.streetAddress == null || req.body.postalCode == null || req.body.city == null || req.body.country == null || req.body.streetAddress.length == 0 || req.body.postalCode.length == 0 || req.body.country.length == 0 || req.body.city.length == 0) {
                        message += 'Your address is incomplete : street, postal code, city and country are required. ';
                        check = false;
                    } else if (req.body.streetAddress.length < 5 || req.body.postalCode.length < 5 || req.body.postalCode.length > 6 || req.body.country.length < 3 || req.body.city.length < 1) {
                        message += 'Your address is invalid. ';
                        check = false;
                    }
                }
                if (req.body.postalCode != null && req.body.postalCode.length < 1 && req.body.postalCode != parseInt(req.body.postalCode)) {
                    message += 'Postal code must be a valid number. ';
                    check = false;
                }

                if (req.body.name == null || req.body.name.length == 0) req.body.name = null;
                if (req.body.email == null || req.body.email.length == 0) req.body.email = null;
                if (req.body.firstName == null || req.body.firstName.length == 0) req.body.firstName = null;
                if (req.body.lastName == null || req.body.lastName.length == 0) req.body.lastName = null;
                if (req.body.streetAddress == null || req.body.streetAddress.length == 0) req.body.streetAddress = null;
                if (req.body.complementAddress == null || req.body.complementAddress.length == 0) req.body.complementAddress = null;
                if (req.body.postalCode == null || req.body.postalCode.length == 0) req.body.postalCode = null;
                if (req.body.city == null || req.body.city.length == 0) req.body.city = null;
                if (req.body.state == null || req.body.state.length == 0) req.body.state = null;
                if (req.body.country == null || req.body.country.length == 0) req.body.country = null;

                if (check) {
                    var date = new Date();
                    User.findByIdAndUpdate(req.body.id, {
                        $set: {
                            name: req.body.name,
                            email: req.body.email,
                            firstName: req.body.firstName,
                            lastName: req.body.lastName,
                            streetAddress: req.body.streetAddress,
                            complementAddress: req.body.complementAddress,
                            postalCode: req.body.postalCode,
                            city: req.body.city,
                            state: req.body.state,
                            country: req.body.country,
                            updateDate: date
                        }
                    }, function (e, user) {
                        if (e) {
                            res.status(400).json({message: "An error occurs"});
                        } else {
                            User.findById(req.body.id, function (e, user) {
                                res.json({message: "Your profile has been updated", user: user});
                            });
                        }
                    });
                } else {
                    res.status(404).json({message: message});
                }
            } else {
                res.status(400).json({message: 'Please type all fields to update profile'});
            }
        });
    },

    updatePassword: function(req, res) {
        if (req.body.oldPassword && req.body.newPassword && req.body.conf_newPassword) {
            User.findById(req.body.id, function (e, user) {
                if (bcrypt.compareSync(req.body.oldPassword, user.password)) {
                    var check = true;
                    var message = '';

                    if (req.body.newPassword != req.body.conf_newPassword || req.body.newPassword.length < 8) {
                        check = false;
                        message += 'Invalid confirmation password or length given (8 characters needed)';
                    }

                    if (check) {
                        var date = new Date();
                        User.findByIdAndUpdate(req.body.id, {
                            $set: {
                                password: bcrypt.hashSync(req.body.newPassword, salt),
                                updateDate: date
                            }
                        }, function (e, user) {
                            if (e) {
                                res.status(400).json({message: "An error occurs"});
                            } else {
                                User.findById(req.body.id, function (e, user) {
                                    res.status(200).json({message: "Your password has been updated", user: user});
                                });
                            }
                        });
                    } else {
                        res.status(404).json({message: message});
                    }
                } else {
                    res.status(404).json({message: 'Wrong password for present login'});
                }
            });
        } else {
            res.status(400).json({message: 'Please type all fields to update password'});
        }
    },

    unRegister: function(req, res) {
        var date = new Date();
        User.findByIdAndUpdate(req.body.id, {$set: {status: false, deleteDate: date}}, function (e, user) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                res.json({message: "The user has been deleted", user: user});
            }
        });
    },

    uploadAvatar: function(req, res) {
        var file = req.files.file;
        var date = new Date();
        var newPath = '/upload/avatar/' + req.body.userId + '.' + file.type.split('/')[1];
        mv(file.path, __dirname + '/../../public/' + newPath, function (e) {
            if (e) {
                res.status(400).json({message: "An error occurs"});
            } else {
                User.findByIdAndUpdate(req.body.userId, {$set: { updateDate: date, avatar: newPath }}, function (e, result) {
                    if (e) {
                        res.status(400).json({message: "An error occurs"});
                    } else {
                        res.json({message: "Your avatar has been uploaded"});
                    }
                });
            }
        });
    }
};

module.exports = ctrlAuth;
