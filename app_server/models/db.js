var mongoose = require('mongoose');

// heroku environment
// var options = { server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } },
//     replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS : 30000 } } };
// mongoose.connect('mongodb://heroku_hntg1pkg:s6322hot9rn3umr3jc3e0dq948@ds011664.mlab.com:11664/heroku_hntg1pkg', options);

// local environment
mongoose.connect('mongodb://localhost/ghosttrain');

var db = mongoose.connection;
db.on('error', console.error);

module.exports = db;