var mongoose = require('mongoose');
var db = require('./db');
var ObjectId = mongoose.Schema.Types.ObjectId;

var postSchema = mongoose.Schema({
        content: { type: String, min: 20, max: 140, required: true },
        author: { type: ObjectId, required: true },
        wall: { type: ObjectId, required: true },
        //link: String,
        status: { type: Boolean, default: true },
        like: [{
                userId: ObjectId
        }],
        comments: [{
                commentId: ObjectId
        }],
        createDate: { type: Date, default: Date.now },
        updateDate: { type: Date, default: Date.now },
        deleteDate: Date
});

var Post = db.model('Post', postSchema);

module.exports = Post;
