var mongoose = require('mongoose');
var db = require('./db');
var ObjectId = mongoose.Schema.Types.ObjectId;

var albumSchema = mongoose.Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    owner: {type: ObjectId, required: true},
    images: [{
        image: ObjectId,
    }],
    like: [{
        userId: ObjectId
    }],
    comments: [{
        commentId: ObjectId
    }],
    status: {type: Boolean, default: true},
    createDate: {type: Date, default: Date.now},
    updateDate: {type: Date, default: Date.now},
    deleteDate: Date
});

var Album = db.model('Album', albumSchema);

module.exports = Album;
