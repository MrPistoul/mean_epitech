var mongoose = require('mongoose');
var db = require('./db');
var ObjectId = mongoose.Schema.Types.ObjectId;

var messageSchema = mongoose.Schema({
        content: { type: String, min: 20, max: 140, required: true },
        type: {type: String, enum: ['friend request', 'friendship response','private message', 'post message', 'album message']},
        author: { type: ObjectId, required: true },
        recipient: { type: ObjectId, required: true },
        status: { type: Boolean, default: true },
        createDate: { type: Date, default: Date.now },
        deleteDate: Date
});

var Message = db.model('Message', messageSchema);

module.exports = Message;
