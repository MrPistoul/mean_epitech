/**
 * Created by seb on 02/06/16.
 */
var mongoose = require('mongoose');
var db = require('./db');
var ObjectId = mongoose.Schema.Types.ObjectId;

var ChatSchema = mongoose.Schema({
    created: Date,
    content: String,
    avatar: String,
    username: String,
    room: String
});

var Chat = mongoose.model('Chat', ChatSchema);

module.exports = Chat;