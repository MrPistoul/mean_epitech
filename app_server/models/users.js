var mongoose = require('mongoose');
var db = require('./db');
var ObjectId = mongoose.Schema.Types.ObjectId;

var userSchema = mongoose.Schema({
    name: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true},
    firstName: String,
    lastName: String,
    streetAddress: String,
    complementAddress: String,
    postalCode: Number,
    city: String,
    state: String,
    country: String,
    friends: [{
        user: ObjectId,
        status: {type: Boolean, default: false},
        process: String,
        createDate: {type: Date, default: Date.now},
        updateDate: {type: Date, default: Date.now},
    }],
    archivedFriends: [{
        user: ObjectId,
        process: String,
        deleteDate: Date
    }],
    avatar: String,
    admin: {type: Boolean, default: false},
    status: {type: Boolean, default: true},
    createDate: {type: Date, default: Date.now},
    updateDate: {type: Date, default: Date.now},
    deleteDate: Date
});

var User = db.model('User', userSchema);

module.exports = User;
