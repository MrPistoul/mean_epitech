var mongoose = require('mongoose');
var db = require('./db');
var ObjectId = mongoose.Schema.Types.ObjectId;

var commentSchema = mongoose.Schema({
    content: { type: String, min: 5, max: 140, required: true },
    author: { type: ObjectId, required: true },
    post: ObjectId,
    wall: ObjectId,
    image: ObjectId,
    album: ObjectId,
    owner: ObjectId,
    status: { type: Boolean, default: true },
    like: [{
        userId: ObjectId
    }],
    createDate: { type: Date, default: Date.now },
    deleteDate: Date
});

var Comment = db.model('Comment', commentSchema);

module.exports = Comment;
