var mongoose = require('mongoose');
var db = require('./db');
var ObjectId = mongoose.Schema.Types.ObjectId;

var imageSchema = mongoose.Schema({
    title: {type: String, required: true},
    path: {type: String, required: true},
    album: {type: ObjectId, required: true},
    owner: {type: ObjectId, required: true},
    like: [{
        userId: ObjectId
    }],
    comments: [{
        commentId: ObjectId
    }],
    status: {type: Boolean, default: true},
    createDate: {type: Date, default: Date.now},
    deleteDate: Date
});

var Image = db.model('Image', imageSchema);

module.exports = Image;
