var mongoose = require('mongoose');
var db = require('./db');
var ObjectId = mongoose.Schema.Types.ObjectId;

var socketSchema = mongoose.Schema({
    socketId: {type: String, required: true},
    userId: {type: ObjectId, required: true}
});

var Socket = db.model('Socket', socketSchema);

module.exports = Socket;
