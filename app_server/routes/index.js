var express = require('express');
var path = require('path');
var router = express.Router();
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var ctrlUsers = require('../controllers/users');
var ctrlAuth = require('../controllers/authentication');
var ctrlPosts = require('../controllers/posts');
var ctrlComments = require('../controllers/comments');
var ctrlAdmin = require('../controllers/admin');
var ctrlMessages = require('../controllers/messages');
var ctrlAlbums = require('../controllers/albums');
var ctrlImages = require('../controllers/images');
var ctrlSockets = require('../controllers/sockets');
var ctrlChat = require('../controllers/chat')

router
/* GET home page. */
    .get('*', function (req, res, next) {
        res.sendFile(path.resolve('public/index.html'));
    })
    /* AUTHENTICATION */
    .post('/register', ctrlAuth.register)
    .post('/login', ctrlAuth.login)
    .post('/updateProfile', ctrlAuth.updateProfile)
    .post('/updatePassword', ctrlAuth.updatePassword)
    .post('/unRegister', ctrlAuth.unRegister)
    .post('/upload/avatar', multipartyMiddleware, ctrlAuth.uploadAvatar)

    /* USERS */
    .put('/user', ctrlUsers.usersReadOne)
    .put('/users', ctrlUsers.usersReadAll)
    .put('/users/friends', ctrlUsers.usersListFriends)
    .put('/users/connectedFriends', ctrlUsers.usersListConnectedFriends)
    .post('/users/requestFriend', ctrlUsers.usersRequestFriend)
    .post('/users/addFriend', ctrlUsers.usersAddFriend)
    .post('/users/rejectFriend', ctrlUsers.usersRejectFriend)
    .post('/users/removeFriend', ctrlUsers.usersRemoveFriend)

    /* POSTS */
    .put('/posts', ctrlPosts.postsReadAll)
    .post('/posts', ctrlPosts.postsAddOne)
    .post('/posts/update', ctrlPosts.postsUpdateOne)
    .post('/posts/like', ctrlPosts.postsAddLike)
    .post('/posts/unlike', ctrlPosts.postsRemoveLike)
    .post('/posts/remove', ctrlPosts.postsDeleteOne)

    /* COMMENTS */
    .put('/comments', ctrlComments.commentsReadAll)
    .put('/comments/albums', ctrlComments.commentsReadAllOnAlbums)
    .put('/comments/album', ctrlComments.commentsReadAllForAnAlbum)
    .post('/comments', ctrlComments.commentsAddOne)
    .post('/comments/album', ctrlComments.commentsAddOneOnAlbum)
    .post('/comments/image', ctrlComments.commentsAddOneOnImage)
    .post('/comments/like', ctrlComments.commentsAddLike)
    .post('/comments/unlike', ctrlComments.commentsRemoveLike)
    .post('/comments/remove', ctrlComments.commentsDeleteOne)

    /* MESSAGES */
    .put('/messages', ctrlMessages.messagesListAllActive)
    .put('/messages/old', ctrlMessages.messagesListAllArchived)
    .post('/messages', ctrlMessages.messagesAddOne)
    .post('/messages/readMessage', ctrlMessages.messagesReadOne)

    /* ALBUMS */
    .put('/albums', ctrlAlbums.albumsListAll)
    .post('/albums', ctrlAlbums.albumsAddOne)
    .post('/albums/update', ctrlAlbums.albumsUpdateOne)
    .post('/albums/like', ctrlAlbums.albumsAddLike)
    .post('/albums/unlike', ctrlAlbums.albumsRemoveLike)
    .post('/albums/remove', ctrlAlbums.albumsDeleteOne)

    /* IMAGES */
    .put('/album', ctrlImages.imagesShowOneAlbum)
    .post('/upload/albums', multipartyMiddleware, ctrlImages.imagesAddOne)
    .post('/images/like', ctrlImages.imagesAddLike)
    .post('/images/unlike', ctrlImages.imagesRemoveLike)
    .post('/images/remove', ctrlImages.imagesDeleteOne)

    /* SOCKETS */
    .put('/sockets', ctrlSockets.socketsFindOne)
    .post('/sockets/remove', ctrlSockets.socketsRemoveOne)

    /* ADMIN */
    .put('/admin/users', ctrlAdmin.adminListUsers)
    .post('/admin/banishUser', ctrlAdmin.adminBanishUser)
    .post('/admin/activateUser', ctrlAdmin.adminActivateUser)
    .put('/admin/posts', ctrlAdmin.adminListPosts)
    .post('/admin/banishPost', ctrlAdmin.adminBanishPost)
    .post('/admin/activatePost', ctrlAdmin.adminActivatePost)
    
    /* CHAT */
    .post('/chat/messages', ctrlChat.messages);
module.exports = router;
