var app = angular
    .module('GhostTrain', ['ngRoute', 'LocalStorageModule', 'ngFileUpload', 'hashtagify', 'ui.bootstrap', 'ui-notification', 'ngMaterial', 'ngAnimate', 'ngMdIcons','btford.socket-io'])
    .config(['$routeProvider', '$locationProvider', 'localStorageServiceProvider', 'NotificationProvider',
        function($routeProvider, $locationProvider, localStorageServiceProvider, NotificationProvider) {
            $routeProvider
                .when('/login', {
                    templateUrl: '../views/login.html',
                    controller: 'loginCtrl'
                })
                .when('/register', {
                    templateUrl: '../views/register.html',
                    controller: 'registerCtrl'
                })
                .when('/logout', {
                    templateUrl: '../views/login.html',
                    controller: 'logoutCtrl'
                })
                .when('/wall', {
                    templateUrl: '../views/wall.html',
                    controller: 'wallCtrl'
                })
                .when('/friends', {
                    templateUrl: '../views/friends.html',
                    controller: 'friendsCtrl'
                })
                .when('/passengers', {
                    templateUrl: '../views/passengers.html',
                    controller: 'passengersCtrl'
                })
                .when('/passenger', {
                    templateUrl: '../views/passenger.html'
                })
                .when('/messages', {
                    templateUrl: '../views/messages.html',
                    controller: 'messagesCtrl'
                })
                .when('/messages/archive', {
                    templateUrl: '../views/messages-archive.html',
                    controller: 'archivedMessagesCtrl'
                })
                .when('/messages/private', {
                    templateUrl: '../views/messages-private.html',
                    controller: 'privateMessageCtrl'
                })
                .when('/albums', {
                    templateUrl: '../views/albums.html',
                    controller: 'albumsCtrl'
                })
                .when('/album', {
                    templateUrl: '../views/album.html',
                    controller: 'albumCtrl'
                })
                .when('/profile', {
                    templateUrl: '../views/profile.html',
                    controller: 'profileCtrl'
                })
                .when('/password', {
                    templateUrl: '../views/password.html',
                    controller: 'profileCtrl'
                })
                .when('/admin/users', {
                    templateUrl: '../views/admin-users.html',
                    controller: 'adminUsersCtrl'
                })
                .when('/admin/posts', {
                    templateUrl: '../views/admin-posts.html',
                    controller: 'adminPostsCtrl'
                })
                .when('/chat', {
                    templateUrl: '../views/chat.html',
                    controller: 'ChatCtrl'
                })
                .otherwise({
                    redirectTo: '/login'
                });

            localStorageServiceProvider
                .setPrefix('GhostTrain')
                .setStorageType('sessionStorage')
                .setStorageCookie(30, '/')
                .setNotify(true, true);

            $locationProvider.html5Mode(true);

            NotificationProvider.setOptions({
                delay: 10000,
                startTop: 20,
                startRight: 10,
                verticalSpacing: 20,
                horizontalSpacing: 20,
                positionX: 'right',
                positionY: 'bottom'
            });
    }]);

app.controller('adminPostsCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        var socket = io.connect();
        var loadData = function () {
            $http.put('/admin/posts')
                .success(function(data, success) {
                    $rootScope.posts = data.posts;
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.admin == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.banishPost = function(id) {
            var data = { id: id };
            $http.post('/admin/banishPost', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.activatePost = function(id) {
            var data = { id: id };
            $http.post('/admin/activatePost', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);

app.controller('adminUsersCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        var socket = io.connect();
        var loadData = function () {
            $http.put('/admin/users')
                .success(function(data, success) {
                    $rootScope.users = data.users;
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.admin == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.banishUser = function(id) {
            var data = { id: id };
            $http.post('/admin/banishUser', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.activateUser = function(id) {
            var data = { id: id };
            $http.post('/admin/activateUser', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);

app.controller('albumCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', '$sce', 'Upload', '$timeout', 'Notification',
    function($rootScope, $scope, $http, $location, $route, $sce, Upload, $timeout, Notification) {
        var socket = io.connect();
        var loadData = function () {
            var data = { id: $rootScope.albumToShow._id };
            $http.put('/album', data)
                .success(function(data, success) {
                    data.images.forEach(function(image) {
                        var isLiked = false;
                        image.like.forEach(function(userId) {
                            if (userId.userId == $rootScope.user._id) {
                                isLiked = true;
                            }
                        });
                        image.isLiked = isLiked;
                    });
                    $rootScope.images = data.images;

                    data = { album: $rootScope.albumToShow._id };
                    $http.put('/comments/album', data)
                        .success(function(data, success) {
                            data.comments.forEach(function (comment) {
                                var isLiked = false;
                                comment.like.forEach(function (userId) {
                                    if (userId.userId == $rootScope.user._id) {
                                        isLiked = true;
                                    }
                                });
                                comment.isLiked = isLiked;
                            });
                            $rootScope.comments = data.comments;
                        })
                        .error(function(data) {
                            Notification({ message: "An error occurs..." }, 'error' );
                        });

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            if ($rootScope.wall == null) {
                $rootScope.wall = $rootScope.user;
            }
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.updateAlbum = function(id, newTitle, newDescription) {
            var data = { title: newTitle, description: newDescription, id: id };
            $http.post('/albums/update', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.removeAlbum = function(id) {
            var data = { id: id };
            $http.post('/albums/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $location.path('/albums');
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.uploadImage = function(file, title) {
            file.upload = Upload.upload({
                url: '/upload/albums',
                method: 'POST',
                data: { file: file, user: $rootScope.user, album: $rootScope.albumToShow, title: title },
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    // NOTIFY FRIENDS
                    var notification = { title: 'album message', message: $rootScope.user.name + ' has published a new photo on ' + $rootScope.albumToShow.title + ' album', type:'primary' };
                    $rootScope.user.friends.forEach(function(friend) {
                        $http.put('/user', {id: friend.user})
                            .success(function(data, status) {
                                var missive = { user: data.user, notification: notification };
                                socket.emit('notification', missive);
                            });
                    });
                    loadData();
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        };

        $scope.removeImage = function(id) {
            var data = { id: id, albumId: $rootScope.albumToShow._id };
            $http.post('/images/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.commentateImage = function(id) {
            $rootScope.imageToComment = id;
            loadData();
        };

        $scope.openImage = function(path) {
            window.open(path, '_blank');
        };

        $scope.likeImage = function(id) {
            var data = { id: id, userId: $rootScope.user._id };
            $http.post('/images/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlikeImage = function(id) {
            var data = { id: id, userId: $rootScope.user._id };
            $http.post('/images/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.postComment = function(id, commentContent, imageOwner, albumId) {
            var data = { content: commentContent, imageId: id, author: $rootScope.user._id, imageOwner: imageOwner, albumId: albumId };
            $http.post('/comments/image', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.commentContent= '';
                    $rootScope.imageToComment = '';
                    if ($rootScope.user._id != imageOwner) {
                        // notification to image owner
                        $http.put('/user', {id: imageOwner})
                            .success(function(data, status) {
                                var notification = { title: 'comment notification', message: $rootScope.user.name + ' has commented one of your images', type:'primary' };
                                var missive = { user: data.user, notification: notification };
                                socket.emit('notification', missive);
                            });
                    }
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.removeComment = function(id) {
            var data = { id: id };
            $http.post('/comments/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.likeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlikeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.tagUserClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.tagTermClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.trustHtml = function(html) {
            return $sce.trustAsHtml(html);
        };

        $scope.clean = function() {
            $rootScope.searchText = '';
        };
    }]);

app.controller('albumsCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', '$sce', 'Notification',
    function($rootScope, $scope, $http, $location, $route, $sce, Notification) {
        var socket = io.connect();
        var loadData = function () {
            var data = { owner: $rootScope.wall._id };
            $http.put('/albums', data)
                .success(function(data, success) {
                    data.albums.forEach(function(album) {
                        var isLiked = false;
                        album.like.forEach(function(userId) {
                            if (userId.userId == $rootScope.user._id) {
                                isLiked = true;
                            }
                        });
                        album.isLiked = isLiked;

                        var data = { id: album._id};
                        $http.put('/album', data)
                            .success(function(data, success) {
                                album.image = data.images[0];
                            })
                            .error(function(data) {
                                Notification({ message: "An error occurs..." }, 'error' );
                            });
                    });
                    $rootScope.albums = data.albums;

                    data = { owner: $rootScope.wall._id };
                    $http.put('/comments/albums', data)
                        .success(function(data, success) {
                            data.comments.forEach(function (comment) {
                                var isLiked = false;
                                comment.like.forEach(function (userId) {
                                    if (userId.userId == $rootScope.user._id) {
                                        isLiked = true;
                                    }
                                });
                                comment.isLiked = isLiked;
                            });
                            $rootScope.comments = data.comments;
                        })
                        .error(function(data) {
                            Notification({ message: "An error occurs..." }, 'error' );
                        });

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            if ($rootScope.wall == null) {
                $rootScope.wall = $rootScope.user;
            }
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.addAlbum = function(title, description) {
            var data = { title: title, description: description, owner: $rootScope.user };
            $http.post('/albums', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.title= '';
                    $scope.description= '';
                    // NOTIFY FRIENDS
                    var notification = { title: 'album message', message: $rootScope.user.name + ' has published a new album', type:'primary' };
                    $rootScope.user.friends.forEach(function(friend) {
                        var missive = { user: friend.user, notification: notification };
                        socket.emit('notification', missive);
                    });
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.commentateAlbum = function(id) {
            $rootScope.albumToComment = id;
            loadData();
        };

        $scope.likeAlbum = function(id) {
            var data = { id: id, userId: $rootScope.user._id };
            $http.post('/albums/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlikeAlbum = function(id) {
            var data = { id: id, userId: $rootScope.user._id };
            $http.post('/albums/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.postComment = function(id, commentContent, albumOwner) {
            var data = { content: commentContent, albumId: id, author: $rootScope.user._id, albumOwner: albumOwner };
            $http.post('/comments/album', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.commentContent= '';
                    $rootScope.albumToComment = '';
                    if ($rootScope.user._id != albumOwner) {
                        // notification to album owner
                        $http.put('/user', {id: albumOwner})
                            .success(function(data, status) {
                                var notification = { title: 'comment notification', message: $rootScope.user.name + ' has commented one of your albums', type:'primary' };
                                var missive = { user: data.user, notification: notification };
                                socket.emit('notification', missive);
                            });
                    }
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.removeComment = function(id) {
            var data = { id: id };
            $http.post('/comments/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.likeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlikeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.tagUserClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.tagTermClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.trustHtml = function(html) {
            return $sce.trustAsHtml(html);
        };

        $scope.clean = function() {
            $rootScope.searchText = '';
        };

        $scope.showAlbum = function(album) {
            $rootScope.albumToShow = album;
            $location.path('/album');
        };

        $scope.showMyAlbums = function() {
            $rootScope.wall = $rootScope.user;
            loadData();
        };

        $scope.sendPrivateMessage = function() {
            $rootScope.recipient = $rootScope.wall;
            $location.path('/messages/private');
        };
    }]);

app.controller('archivedMessagesCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        $rootScope.recipient = null;
        var socket = io.connect();
        var loadData = function () {
            var data = { id : $rootScope.user._id};
            $http.put('/messages/old', data)
                .success(function(data, success) {
                    $rootScope.messages = data.messages;

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.replyMessage = function(messageId, user) {
            $rootScope.recipient = user;
            $location.path('messages/private');
        };
    }]);

app.controller('ChatCtrl', ['$rootScope', '$scope', '$mdDialog', '$http', '$location', 'socket',
    function ($rootScope, $scope, $mdDialog, $http, $location, socket) {
        // var socket = io.connect();
        if ($rootScope.user != null) {
            $scope.messages = [];
            $scope.username = $rootScope.user.name;
        } else {
            $location.path('/login');
        }

        socket.on('message created', function (data) {
            $scope.messages.push(data);
            });

        $scope.StartChat = function (event) {
            var answer = $rootScope.user.name;
            $rootScope.room = $scope.room;
            socket.emit('new user', {
                username: answer,
                room: $scope.room
            });
            $http.put('/users/connectedFriends', { id: $rootScope.user._id })
                .success(function(data, status) {
                    data.users.forEach(function(user) {
                        var notification = { title: 'chat notification', message: $rootScope.user.name + ' has join ' + $rootScope.room + ' chat room', type:'primary' };
                        var missive = { user: user, notification: notification };
                        socket.emit('notification', missive);
                    });
                });
            var data = {room: $rootScope.room};
            $http.post('chat/messages', data).success(function (msgs) {
                $scope.messages = msgs;
            });
            $location.path('/chat');
        };

        $scope.send = function (msg) {
            socket.emit('new message', {
                room: $scope.room,
                message: msg,
                username: $rootScope.user.name,
                avatar: $rootScope.user.avatar
            });
    };
}])
    .directive('ngEnter', function () {
        return function ($scope, element, attrs) {
            element.bind("keypress", function (event) {
                if (event.which === 13) {
                    $scope.$apply(function () {
                        $scope.$eval(attrs.ngEnter);
                    });
                    event.preventDefault();
                    $scope.message = '';
                }
            });
        };
    })
    .factory('socket', function (socketFactory) {
        var myIoSocket = io.connect();
    
        var socket = socketFactory({
            ioSocket: myIoSocket
        });
        return socket;
    });

app.controller('friendsCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        var socket = io.connect();
        var loadData = function () {
            var data = { id : $rootScope.user._id};
            $http.put('/users/friends', data)
                .success(function(data, success) {
                    data.users.forEach(function (user) {
                        user.friends.forEach(function (friend) {
                            if (friend.user && friend.user == $rootScope.user._id) {
                                user.friendshipDate = friend.createDate;
                            }
                        });
                    });
                    $rootScope.friends = data.users;

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.showFriend = function(user) {
            $rootScope.wall = user;
            $location.path('/wall');
        };

        $scope.removeFriend = function(id) {
            var data = {
                id: $rootScope.user._id,
                anonymous: id
            };
            $http.post('/users/removeFriend', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);

app.controller('loginCtrl', ['$rootScope', '$scope', '$http', 'localStorageService', '$location', 'Notification',
    function($rootScope, $scope, $http, localStorageService, $location, Notification) {
        var checkStorage = localStorageService.isSupported;
        var checkCookie = localStorageService.cookie.isSupported;
        var socket = io.connect();

        if (checkStorage) {
            if (localStorageService.get("user")) {
                $rootScope.user = localStorageService.get("user");
                $location.path('/wall');
            } else if (checkCookie && localStorageService.cookie.get("user")) {
                $rootScope.user = localStorageService.cookie.get("user");
                $location.path('/wall');
            }
        }

        $scope.login = function() {
            var data = { login: $scope.loginEmail, password: $scope.loginPassword };
            var remember = $scope.rememberMe;
            $http.post('/login', data)
                .success(function(data, status) {
                    $rootScope.user = data.user;
                    Notification({ message: data.message }, 'success' );
                    $scope.loginEmail = '';
                    $scope.loginPassword = '';
                    var redirect = function($location) {
                        $location.path('/wall');
                        var missive = { user: $rootScope.user };
                        socket.emit('login', missive);
                    };
                    var setUser = function($scope, checkStorage, localStorageService, $location, data, redirect) {
                        if(checkStorage) {
                            localStorageService.set("user", data.user);
                        }
                        if(remember && checkCookie) {
                            localStorageService.cookie.set("user", data.user);
                        }
                        redirect($location);
                    };
                    setUser($scope, checkStorage, localStorageService, $location, data, redirect);

                    // SOCKET LISTENING FOR NOTIFICATION
                    socket.on('notification', function(data) {
                        Notification({ title: data.title, message: data.message }, data.type );
                    });

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });

                    // GO BACK
                    $rootScope.goBack = function(){
                        history.back();
                    }
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                    $scope.loginPassword = '';
                });
        };

        $scope.goToRegister = function() {
            $location.path('/register');
        };
    }]);

app.controller('logoutCtrl', ['$rootScope', 'localStorageService', '$location', 'Notification', '$http',
    function($rootScope, localStorageService, $location, Notification, $http) {
        var checkStorage = localStorageService.isSupported;
        var checkCookie = localStorageService.cookie.isSupported;
        var data = { userId: $rootScope.user._id};

        var logout = function(localStorageService, $location) {
            $rootScope.wall = null;
            $rootScope.user = null;
            Notification({ message: "you've been successfully disconnected" }, 'success' );
            if(checkStorage) {
                localStorageService.remove("user");
            }
            if(checkCookie) {
                localStorageService.cookie.remove("user");
            }

            $location.path('/login');
        };
        logout(localStorageService, $location);

        $http.post('/sockets/remove', data);
    }]);

app.controller('messagesCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        $rootScope.recipient = null;
        var socket = io.connect();
        var loadData = function () {
            var data = { id : $rootScope.user._id};
            $http.put('/messages', data)
                .success(function(data, success) {
                    $rootScope.messages = data.messages;

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.addFriend = function(messageId, id) {
            var data = {
                id: $rootScope.user._id,
                applicant: id,
                messageId: messageId
            };
            $http.post('/users/addFriend', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    // NOTIFY THE APPLICANT
                    $http.put('/user', {id: id})
                        .success(function(data, status) {
                            var notification = { title: 'friendship response', message: $rootScope.user.name + ' agreed to be your friend', type:'primary' };
                            var missive = { user: data.user, notification: notification };
                            socket.emit('notification', missive);
                        });
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.rejectFriend = function(messageId, id) {
            var data = {
                id: $rootScope.user._id,
                applicant: id,
                messageId: messageId
            };
            $http.post('/users/rejectFriend', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    // NOTIFY THE APPLICANT
                    $http.put('/user', {id: id})
                        .success(function(data, status) {
                            var notification = { title: 'friendship response', message: $rootScope.user.name + ' don\'t want to be your friend', type:'warning' };
                            var missive = { user: data.user, notification: notification };
                            socket.emit('notification', missive);
                        });
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.readMessage = function(messageId) {
            var data = { id: messageId };
            $http.post('/messages/readMessage', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.replyMessage = function(messageId, user) {
            $rootScope.recipient = user;
            var data = { id: messageId };
            $http.post('/messages/readMessage', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $location.path('messages/private');
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);

app.controller('passengersCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        var socket = io.connect();
        var loadData = function () {
            var data = { id : $rootScope.user._id};
            $http.put('/users', data)
                .success(function(data, success) {
                    data.users.forEach(function(user) {
                        user.friends.forEach(function (friend) {
                            if (friend.user && friend.user == $rootScope.user._id && (friend.process == "accepted" || friend.process == "agreed")) {
                                user.friendship = true;
                            } else if (friend.user && friend.user == $rootScope.user._id && (friend.process == "request" || friend.process == "demand")) {
                                user.friendship = "in progress";
                            }
                        });
                    });
                    $rootScope.users = data.users;

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            loadData();
        } else {
            $location.path('/login');
        }

        $scope.requestFriend = function(id) {
            var data = { id: $rootScope.user._id, candidate: id };
            $http.post('/users/requestFriend', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    // NOTIFY THE CANDIDATE
                    $http.put('/user', {id: id})
                        .success(function(data, status) {
                            var notification = { title: 'friend request', message: $rootScope.user.name + ' want to be your friend', type:'primary' };
                            var missive = { user: data.user, notification: notification };
                            socket.emit('notification', missive);
                        });
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.showPassenger = function(id) {
            var data = { id: id };
            $http.put('/user', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $rootScope.passenger = data.user;
                    $location.path('/passenger');
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);

app.controller('privateMessageCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Notification) {
        var socket = io.connect();
        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
        } else {
            $location.path('/login');
        }
        $scope.addMessage = function(content) {
            var data = {
                from: $rootScope.user._id,
                to: $rootScope.recipient._id,
                content: content,
                type: 'private message'
            };
            $http.post('/messages', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    // NOTIFY THE APPLICANT
                    var notification = { title: 'private message', message: $rootScope.user.name + ' has sent you a private message', type:'primary' };
                    var missive = { user: $rootScope.recipient._id, notification: notification };
                    socket.emit('notification', missive);
                    history.back();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);

app.controller('profileCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', 'Upload', '$timeout', 'Notification',
    function($rootScope, $scope, $http, $location, $route, Upload, $timeout, Notification) {
        var socket = io.connect();
        var missive = { user: $rootScope.user };
        socket.emit('login', missive);
        if ($rootScope.user == null || $rootScope.user.status == false) {
            $location.path('/login');
        }

        // BADGE REFRESHING
        data = { id: $rootScope.user._id };
        $http.put('/messages', data)
            .success(function(data, status) {
                $rootScope.badge = data.messages.length;
            });

        $scope.updateProfile = function() {
            var data = {
                id: $rootScope.user._id,
                name: $scope.name,
                email: $scope.email,
                firstName: $scope.firstName,
                lastName: $scope.lastName,
                streetAddress: $scope.streetAddress,
                complementAddress: $scope.complementAddress,
                postalCode: $scope.postalCode,
                city: $scope.city,
                state: $scope.state,
                country: $scope.country };
            console.log(data);
            $http.post('/updateProfile', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $rootScope.user = data.user;
                    $scope.name= '';
                    $scope.email = '';
                    $scope.password = '';
                    $scope.conf_password = '';
                    $route.reload();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                    $scope.password = '';
                    $scope.conf_password = '';
                });
        };

        $scope.updatePassword = function() {
            var data = {
                id: $rootScope.user._id,
                oldPassword: $scope.oldPassword,
                newPassword: $scope.newPassword,
                conf_newPassword: $scope.conf_newPassword };
            $http.post('/updatePassword', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $rootScope.user = data.user;
                    $scope.oldPassword = '';
                    $scope.newPassword = '';
                    $scope.conf_newPassword = '';
                    $route.reload();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                    $scope.oldPassword = '';
                    $scope.newPassword = '';
                    $scope.conf_newPassword = '';
                });
        };

        $scope.unRegister = function() {
            var data = { id: $rootScope.user._id };
            $http.post('unRegister', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $rootScope.user = data.user;
                    $location.path('/logout');
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.uploadAvatar = function(file) {
            file.upload = Upload.upload({
                url: '/upload/avatar',
                method: 'POST',
                data: {userId: $rootScope.user._id},
                file: file
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    updateUser();
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        };

        updateUser = function() {
            var data = { id: $rootScope.user._id };
            $http.put('/user', data)
                .success(function (data, status) {
                    $rootScope.user = data.user;
                    Notification({ message: data.message }, 'success' );
                })
                .error(function (data) {
                    Notification({ message: data.message }, 'error' );
                });
        };
    }]);

app.controller('registerCtrl', ['$rootScope', '$scope', '$http', '$location', 'Notification',
    function($rootScope, $scope, $http, $location, Notification) {
        $scope.register = function() {
            var data = { name: $scope.name, email: $scope.email, password: $scope.password, conf_password: $scope.conf_password };
            $http.post('/register', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.name= '';
                    $scope.email = '';
                    $scope.password = '';
                    $scope.conf_password = '';
                    $location.path('/login');
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                    $scope.password = '';
                    $scope.conf_password = '';
                })
        };
    }]);

app.controller('wallCtrl', ['$rootScope', '$scope', '$http', '$location', '$route', '$sce', 'Notification',
    function($rootScope, $scope, $http, $location, $route, $sce, Notification) {
        var socket = io.connect();
        var loadData = function() {
            var data = { wall: $rootScope.wall._id };
            $http.put('/posts', data)
                .success(function(data, success) {
                    data.posts.forEach(function(post) {
                        var isLiked = false;
                        post.like.forEach(function(userId) {
                            if (userId.userId == $rootScope.user._id) {
                                isLiked = true;
                            }
                        });
                        post.isLiked = isLiked;
                    });
                    $rootScope.posts = data.posts;

                    data = { wall: $rootScope.wall._id };
                    $http.put('/comments', data)
                        .success(function(data, success) {
                            data.comments.forEach(function (comment) {
                                var isLiked = false;
                                comment.like.forEach(function (userId) {
                                    if (userId.userId == $rootScope.user._id) {
                                        isLiked = true;
                                    }
                                });
                                comment.isLiked = isLiked;
                            });
                            $rootScope.comments = data.comments;
                        })
                        .error(function(data) {
                            Notification({ message: "An error occurs..." }, 'error' );
                        });

                    // BADGE REFRESHING
                    data = { id: $rootScope.user._id };
                    $http.put('/messages', data)
                        .success(function(data, status) {
                            $rootScope.badge = data.messages.length;
                        });
                })
                .error(function(data) {
                    Notification({ message: "An error occurs..." }, 'error' );
                });
        };

        if ($rootScope.user != null && $rootScope.user.status == true) {
            var missive = { user: $rootScope.user };
            socket.emit('login', missive);
            if ($rootScope.wall == null) {
                $rootScope.wall = $rootScope.user;
            }
            loadData();
        } else {
            $location.path('/login');
        }


        $scope.post = function() {
            var data = { content: $scope.content, author: $rootScope.user._id, wall: $rootScope.wall._id }; // link to add
            $http.post('/posts', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.content= '';
                    if ($rootScope.user._id != $rootScope.wall._id) {
                        // notification to wall owner
                        var notification = { title: 'post notification', message: $rootScope.user.name + ' has write a new post on your wall', type:'primary' };
                        var missive = { user: $rootScope.wall, notification: notification };
                        socket.emit('notification', missive);
                    }
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.edit = function(id) {
            $rootScope.postToUpdate = id;
            loadData();
        };

        $scope.commentate = function(id) {
            $rootScope.postToComment = id;
            //loadData();
        };

        $scope.update = function(id, newContent) {
            var data = { content: newContent, id: id };
            $http.post('/posts/update', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.newContent= '';
                    $rootScope.postToUpdate = '';
                    if ($rootScope.user._id != $rootScope.wall._id) {
                        // notification to wall owner
                        var notification = { title: 'post notification', message: $rootScope.user.name + ' has update his post on your wall', type:'primary' };
                        var missive = { user: $rootScope.wall, notification: notification };
                        socket.emit('notification', missive);
                    }
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.remove = function(id) {
            var data = { id: id };
            $http.post('/posts/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.like = function(postId) {
            var data = { postId: postId, userId: $rootScope.user._id };
            $http.post('/posts/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlike = function(postId) {
            var data = { postId: postId, userId: $rootScope.user._id };
            $http.post('/posts/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.postComment = function(id, commentContent, postAuthor) {
            var data = { content: commentContent, postId: id, author: $rootScope.user._id, postAuthor: postAuthor._id, wall: $rootScope.wall._id };
            $http.post('/comments', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    $scope.commentContent= '';
                    $rootScope.postToComment = '';
                    if ($rootScope.user._id != $rootScope.wall._id) {
                        // notification to wall owner
                        var notification = { title: 'comment notification', message: $rootScope.user.name + ' has commented a post on your wall', type:'primary' };
                        var missive = { user: $rootScope.wall, notification: notification };
                        socket.emit('notification', missive);
                    }
                    if ($rootScope.user._id != postAuthor._id) {
                        // notification to post owner
                        var notification = { title: 'comment notification', message: $rootScope.user.name + ' has commented your post on ' + $rootScope.wall.name + '\'s wall', type:'primary' };
                        var missive = { user: postAuthor, notification: notification };
                        socket.emit('notification', missive);
                    }
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.removeComment = function(id) {
            var data = { id: id };
            $http.post('/comments/remove', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.likeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/like', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.unlikeComment = function(commentId) {
            var data = { commentId: commentId, userId: $rootScope.user._id };
            $http.post('/comments/unlike', data)
                .success(function(data, status) {
                    Notification({ message: data.message }, 'success' );
                    loadData();
                })
                .error(function(data) {
                    Notification({ message: data.message }, 'error' );
                });
        };

        $scope.showAlbums = function() {
            $location.path('/albums');
        };

        $scope.tagUserClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.tagTermClick = function(e) {
            var tagText = e.target.innerText;
            $rootScope.searchText = tagText;
        };

        $scope.trustHtml = function(html) {
            return $sce.trustAsHtml(html);
        };

        $scope.clean = function() {
            $rootScope.searchText = '';
        };

        $scope.showMyWall = function() {
            $rootScope.wall = $rootScope.user;
            loadData();
        };

        $scope.sendPrivateMessage = function() {
            $rootScope.recipient = $rootScope.wall;
            $location.path('/messages/private');
        };
    }]);
